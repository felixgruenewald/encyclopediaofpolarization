---
authors: J. Pérez-Rajó
date: 2024-01-01
title: Does Populism Fuel Affective Polarization? An Individual-Level Panel Data Analysis
subtitle: 
publication: Political Studies
doi: https://doi.org/10.1177/00323217231224579
polarisation:
  - affective
  - elite
  - vertical
  - horizontal
measures:
  - spread
data:
  - other
aliases:
  - Pérez-Rajó (2024)
tags:
---

## Abstract
Populism and affective polarization speak of a bisected society, and scholars have linked them at the party level. While the relationship between populism and affective polarization can be reciprocal, this article leverages panel data to delve into the causal relationship between populism and affective polarization. First, I posit individuals who become identified with populist parties will hold higher levels of affective polarization. Second, I expect increases in levels of populist attitudes to heighten affective polarization, and this is explained by both the increase of affect towards the ingroup and the decrease of affect towards the outgroup. Using nine waves of a panel survey, results show the importance of differentiating between measures of populism at the individual level, as the increase in populist attitudes appears to fuel affective polarization, symmetrically increasing affect towards the ingroup and decreasing affect towards the outgroup, while the effect of becoming a populist party supporter is more nuanced, as it is different for populist left and right parties.