---
authors: Russell J. Dalton, Carl C. Berning
date: 2022-01-01
title: Ideological Polarization and Far-Right Parties in Europe
subtitle: 
publication: Rechtspopulismus in Deutschland. Wahlverhalten in Zeiten politischer Polarisierung
doi: https://doi.org/10.1007/978-3-658-33787-2_2
polarisation:
  - ideological
  - elite
measures:
  - polarization-index
data:
  - ches
aliases:
  - Dalton and Berning (2022)
---

## Abstract
West European party systems have undergone a systematic realignment of party policy positions over the past several decades. The traditional economic cleavage that initially structured many of these systems is still important, and economic issues continue to define voters’ interests and needs. In addition, a new cultural cleavage has emerged, consisting of issues dealing with social equality, diversity, morality, and related topics. Using the Chapel Hill Expert Surveys (CHES) from 2006 to 2019, we track the evolution of party positions on both cleavages over time. These analyses show the emergence of far-right parties across Western Europe and their policy evolution over time. Far-right parties now generally represent centrist views on economic policy combined with starkly conservative positions on the cultural cleavage. We discuss the implications of these patterns for parties and voters in European party systems.
