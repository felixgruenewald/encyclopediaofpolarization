---
title: Vertical Polarization
weight: 5
---

As [Druckman and Levendusky (2019)]({{< ref "druckman_what_2019" >}}) point out, orientations toward party elites and supporters are conceptually distinct – [Röllicke (2023)]({{< ref "rollicke_polarisation_2023" >}}) refers to measures based on the former as vertical polarization and those based on the latter as horizontal polarization – and may, therefore, also differ empirically. 

Initial findings from the Netherlands ([Harteveld 2021]({{< ref "harteveld_fragmented_2021" >}})), Israel ([Gidron et al. 2022]({{< ref "gidron_validating_2022" >}})), and Romania ([Ciobanu and Sandu 2022]({{< ref "ciobanu_what_2022" >}})) suggest that evaluations of parties and partisans are strongly but not perfectly correlated. Using survey data from Spain, [Comellas Bonsfills (2022)]({{< ref "comellasbonsfills_when_2022" >}}) shows that affective polarization measured by feelings toward parties tends to overestimate the extent to which people dislike voters of opposing parties but that the gap between party and partisan dislike decreases in the ideological distance between partisans. Finally, [Reiljan et al. (2023)]({{< ref "reiljan_patterns_2023" >}}), by computing measures of affective polarization from the [CSES’s]({{< ref "cses" >}}) like-dislike questions on parties and their leaders, show that both are strongly correlated but that affective polarization toward parties is stronger than toward their leaders. In contrast, by using distinct measures for out-party polarization (party thermometer) and out-partisan polarization (social distance measures), [Tichelbaecker et al. (2023)]({{< ref "tichelbaecker_what_2023" >}}) find only a modest relationship between both concepts.

{{< citation >}}

## Measures

| Measure                            | Polarization                           |
| ---------------------------------- | -------------------------------------- |
| [API]({{< ref "measures/api.md" >}})           | affective, mass, vertical              |
| [Distance]({{< ref "measures/distance.md" >}}) | affective, mass, vertical              |
| [Party Dyads]({{< ref "measures/dyads.md" >}}) | ideological, affective, mass, vertical |
| [Spread]({{< ref "measures/spread.md" >}})     | affective, mass, vertical              |

## Use cases

| Title                                                                                                                               | Authors                        |
| ----------------------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| [Can’t We All Just Get Along? How Women MPs Can Ameliorate Affective Polarization in Western Publics]({{< ref "usecases/adams_can_2023.md" >}}) | Adams et al. (2023)            |
| [Elite polarization, party extremity, and affective polarization]({{< ref "usecases/banda_elite_2018.md" >}})                                   | Banda and Cluverius (2018)     |
| [Camps, not just parties]({{< ref "usecases/bantel_camps_2023.md" >}})                                                                          | Bantel (2023)                  |
| [A regional perspective to the study of affective polarisation]({{< ref "usecases/bettarelli_regional_2023.md" >}})                             | Bettarelli et al. (2023)       |
| [What can De-Polarize the Polarizers?]({{< ref "usecases/ciobanu_what_2022.md" >}})                                                             | Ciobanu and Sandu (2022)       |
| [When polarised feelings towards parties spread to voters]({{< ref "usecases/comellasbonsfills_when_2022.md" >}})                               | Comellas Bonsfills (2022)      |
| [What Do We Measure When We Measure Affective Polarization?]({{< ref "usecases/druckman_what_2019.md" >}})                                      | Druckman and Levendusky (2019) |
| [Affective Polarisation in Comparative and Longitudinal Perspective]({{< ref "usecases/garzia_affective_2023.md" >}})                           | Garzia et al. (2023)           |
| [American Affective Polarization in Comparative Perspective]({{< ref "usecases/gidron_american_2020.md" >}})                                    | Gidron et al. (2020)           |
| [Validating the feeling thermometer as a measure of partisan affect in multi-party systems]({{< ref "usecases/gidron_validating_2022.md" >}})   | Gidron et al. (2022)           |
| [Does affective polarisation increase turnout?]({{< ref "usecases/harteveld_does_2023.md" >}})                                                  | Harteveld and Wagner (2023)    |
| [Fragmented foes]({{< ref "usecases/harteveld_fragmented_2021.md" >}})                                                                          | Harteveld (2021)               |
| [Affective polarization and the salience of elections]({{< ref "usecases/hernández_affective_2021.md" >}})                                      | Hernández et al. (2021)        |
| [Affect, Not Ideology]({{< ref "usecases/iyengar_affect_2012.md" >}})                                                                           | Iyengar et al. (2012)          |
| [Sweet victory, bitter defeat]({{< ref "usecases/janssen_sweet_2023.md" >}})                                                                    | Janssen (2023)                 |
| [Affective Polarization and Misinformation Belief]({{< ref "usecases/jenke_affective_2023.md" >}})                                              | Jenke (2023)                   |
| [The relationship between affective polarisation and democratic backsliding]({{< ref "usecases/orhan_relationship_2022.md" >}})                 | Orhan (2022)                   |
| [Fear and loathing across party lines (also) in Europe]({{< ref "usecases/reiljan_fear_2020.md" >}})                                            | Reiljan (2020)                 |
| [Patterns of Affective Polarisation toward Parties and Leaders across the Democratic World]({{< ref "usecases/reiljan_patterns_2023.md" >}})    | Reiljan et al. (2023)          |
| [Overlapping polarization]({{< ref "usecases/riera_overlapping_2023.md" >}})                                                                    | Riera and Madariaga (2023)     |
| [Polarisation, identity and affect]({{< ref "usecases/rollicke_polarisation_2023.md" >}})                                                       | Röllicke (2023)                |
| [Exploring differences in affective polarization between the Nordic countries]({{< ref "usecases/ryan_exploring_2023.md" >}})                   | Ryan (2023)                    |
| [What Do We Measure When We Measure Affective Polarization across Countries?]({{< ref "usecases/tichelbaecker_what_2023.md" >}})                | Tichelbaecker et al. (2023)    |

