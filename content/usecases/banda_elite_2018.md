---
authors: Kevin K. Banda, John Cluverius
date: 2018-01-01
title: Elite polarization, party extremity, and affective polarization
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2018.09.009
polarisation:
  - affective
  - ideological
  - elite
  - mass
  - vertical
measures:
  - range
data:
  - other
aliases:
  - Banda and Cluverius (2018)
---
## Abstract
Elites in the U.S. have become increasingly polarized over the past several decades. More recently, the degree to which partisans view the opposing party more negatively than their own — a phenomenon called affective, or social, polarization — has increased. How does elite polarization inform affective polarization? We argue that partisans respond to increasing levels of elite polarization by expressing higher levels of affective polarization, i.e. more negative evaluations of the opposing party relative to their own. Motivated reasoning further encourages partisans to blame the opposing party more than their own. Results from surveys collected from 1978 through 2016 provide strong support for our theory. We further find that increasing levels of political interest magnify the relationship between elite and affective polarization. These results produce important implications for the health of democratic systems experiencing high levels of elite polarization.
