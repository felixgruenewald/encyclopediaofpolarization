---
authors: Josep M. Comellas, Mariano Torcal
date: 2023-01-01
title: Ideological identity, issue-based ideology and bipolar affective polarization in multiparty systems
subtitle: The cases of Argentina, Chile, Italy, Portugal and Spain
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102615
polarisation:
  - affective
  - mass
  - horizontal
measures:
  - distance
data:
  - other
aliases:
  - Comellas and Torcal (2023)
---

## Abstract
In multiparty contexts, we know that affective polarization tends to cluster in ideological blocs, although the factors driving this process are still quite unexplored. In this paper, we contribute to filling this gap in the literature by exploring the capacity of ideological identity vis-a-vis issue-based ideology to polarize sentiments towards party voters into two opposing left-right blocs. Specifically, we provide empirical evidence that affective attachments to ideological labels increase the affective distance between ideological blocs to a greater extent than issue extremity and issue consistency. These bipolarizing effects of ideological identity persist even when the identity is inconsistent with issue-based ideology. Additionally, we show that bipolar affective polarization exerts little reverse influence on ideological identity. We support these arguments using an original survey from the TRI-POL project carried out in five multiparty systems: Argentina, Chile, Italy, Portugal and Spain.