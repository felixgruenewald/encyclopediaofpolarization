---
authors: Libby Jenke
date: 2023-01-01
title: Affective Polarization and Misinformation Belief
subtitle: 
publication: Political Behavior
doi: https://doi.org/10.1007/s11109-022-09851-w
polarisation:
  - affective
  - mass
  - vertical
measures: 
data:
  - other
aliases:
  - Jenke (2023)
---

## Abstract
While affective polarization has been shown to have serious social consequences, there is little evidence regarding its effects on political attitudes and behavior such as policy preferences, voting, or political information accrual. This paper provides evidence that affective polarization impacts misinformation belief, arguing that citizens with higher levels of affective polarization are more likely to believe in-party-congruent misinformation and less likely to believe out-party-congruent misinformation. The argument is supported by data from the ANES 2020 Social Media Study and the ANES 2020 Time Series Study, which speaks to the generalizability of the relationship. Additionally, a survey experiment provides evidence that the relationship is causal. The results hold among Democrats and Republicans and are independent of the effects of partisan strength and ideological extremity. Furthermore, the relationship between affective polarization and misinformation belief is exacerbated by political sophistication rather than tempered by it, implying that education will not solve the issue. The results speak to the need for work on reducing affective polarization.