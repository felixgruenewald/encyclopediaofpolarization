---
authors: Hans Keman
date: 1997-01-01
title: Centre-Space Politics
subtitle: Party Behaviour in Multi-Party Systems
publication: The Politics of Problem-Solving in Postwar Democracies
doi: https://doi.org/10.1007/978-1-349-25223-7_5
polarisation: 
measures: 
data: 
  - other
aliases:
  - Keman (1997)
---

## Abstract
Party systems in continental European polities represent an institutional arrangement which directs and influences party behaviour. Parties, although considered throughout this book as crucial actors in transforming societal conflicts into political consensus, cannot be analysed in isolation. Party behaviour should be understood as a result of the interactions with other parties within a polity. A party system can be identified by a number of specific (national) features that consist of a set of ‘rules’ directing the patterns of interaction. From perusing the literature on party systems it is clear that — apart from many cross-national differences that are observed — there are many striking similarities between them (Von Beyme, 1985, p. 256ff.; Lane and Ersson, 1994, p. 154ff.; Daalder, 1983; Smith, 1990).
