# Encyclopedia of Polarization

This is the backbone of our website, the encyclopedia of polarization. It is built using hugo and an adaptation of the digital garden theme. The `content` is produced in Obsidian and converted to a hugo compatible format using the `obsidian-to-hugo` python package. 