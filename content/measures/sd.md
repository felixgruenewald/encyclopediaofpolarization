---
title: SD
name: Standard Deviation
polarisation:
  - ideological
  - elite
  - mass
data:
  - cses
  - eb
  - ees
  - ches
datasec:
  - ess
  - marpor
variables:
  - self-positioning
  - party-position
usecases:
  - hobolt_mobilising_2019
  - bischof_voters_2019
  - moral_relationship_2023
  - vegetti_political_2014
  - wilford_polarization_2017
math: true
---
## Description
Of course, polarization, i.e. the distribution of party or voter positions, can also be measured by calculating the standard deviation. It is used by a number of scholars, primarily to measure ideological polarization among the mass public, based on [CSES]({{< ref "cses" >}}), [Eurobarometer]({{< ref "eb" >}}) and [EES]({{< ref "ees" >}}) data.

## Operationalization
The standard deviation of party or voter positions is calculated as follows:
$$s_i = \sqrt{\frac{\sum_{i=1}^n(p_i-\bar{p})^2}{n}}$$
where the subscript $i$ stands for an individual respondent or party, $p_i$ is the position of a respondent or party $i$, $\bar{p}$ is the average position of all respondents or parties, and $n$ is the number of parties or respondents.



## polaR
{{< polarref >}}

A standard deviation can be computed in many different ways, so there are three different functions in polaR to do so. 

```r
# Import Data
cses_imd <- polaR_import(source = "cses_imd",
						 path = "path/to/dataset.dta")
```

For example, an SD can refer to the individual opinion scores of respondents. For this use case, the function `sd_mass` can be used. `issue` specifies which issue dimension should be used, where applicable.

``` r
sd_data <- sd_mass(cses5,
				   issue = "leftright")

sd <- sd_mass(ess,
			 issue = "leftright")
```

Then, there are perceptions of party positions, for which `sd_partyperceptions` computes a standard deviation. This can be done with an individual level SD-variable, or on an aggregated country/year level.

```r
sd_parties_lr_cses5 <- sd_partyperception(cses5,
										  issue = "leftright",
										  aggregate = TRUE)
```

Lastly, there are expert scores of party positions. The function to compute their standard deviation needs to know the structure of the dataset, as expert scores can be found in respondent-based data such as CSES, or party based data like MARPOR. The `units` option can be used to specify between `party` and `respondent`.

```r
sd_expert_lr_cses5 <- sd_experts(cses5,
								 issue =  "leftright",
								 units = "respondent")

sd_expert_galtan_ches <- sd_experts(ches,
									issue = "galtan",
									units = "party")

sd_expert_lr_marpor <- sd_experts(marpor,
								  issue = "leftright",
								  units = "party")
```

## Visualization
{{< shinyapp "cses,eb" "sd_mass,sd_partyperception,sd_expert_parties" >}}

## Use cases
_Publications that use this measure:_

| Title                                                                                                            | Authors                   |
| ---------------------------------------------------------------------------------------------------------------- | ------------------------- |
| [Do Voters Polarize When Radical Parties Enter Parliament?]({{< ref "usecases/bischof_voters_2019.md" >}})                   | Bischof and Wagner (2019) |
| [The mobilising effect of political choice]({{< ref "usecases/hobolt_mobilising_2019.md" >}})                                | Hobolt and Hoerner (2019) |
| [On the relationship between party polarisation and citizen polarisation]({{< ref "usecases/moral_relationship_2023.md" >}}) | Moral and Best (2023)     |
| [From political conflict to partisan evaluations]({{< ref "usecases/vegetti_political_2014.md" >}})                          | Vegetti (2014)            |
| [Polarization, Number of Parties, and Voter Turnout]({{< ref "usecases/wilford_polarization_2017.md" >}})                    | Wilford (2017)            |

