---
title: Range
name: Range Between Outer-Right and -Left Parties
polarisation:
  - ideological
  - elite
data:
  - marpor
  - ches
variables:
  - party-position
usecases:
  - abedi_challenges_2002
  - crepaz_impact_1990
  - matakos_electoral_2016
  - andrews_spatial_2009
  - wessels_meaningful_2008
  - banda_elite_2018
  - mair_reflections_1997
math: true
---
## Description
The range between the two most distant parties on the left-right dimension in a party system is one of the earliest, first used by [Crepaz (1990)]({{< ref "crepaz_impact_1990" >}}), and most widely used measures of ideological polarization. While the measure has mostly been applied to [CHES]({{< ref "ches" >}}) expert ratings and [MARPOR]({{< ref "marpor" >}}) estimates of party positions, it is in principle applicable to any estimates of party positions, regardless of their source.

## Operationalization
The range is defined as follows:
$$range = max(lr_p)-min(lr_p)$$

where $lr_p$ is the left-right position of a party $p$ and the measure captures the difference between the left-most and right-most party, i.e., the range.

## polaR
{{< polarref >}}
```r
# Import Data
cses_imd <- polaR_import(source = "cses_imd",
						 path = "path/to/dataset.dta")


# A range can be computed on the individual level or between already aggregated scores, this can be specified using 'level'
# The data output can be provided with individual scores or aggregated to a country/year level with 'aggregate'
# Where different issue dimensions are available, 'issue' can be issued to specify the one to use
range_data <- range(cses_imd,
					issue = "leftright",
					level = c("individual", "aggregate"),
					aggregate = c(TRUE, FALSE))
```

## Visualization
{{< shinyapp cses "range_ind,range_agg" >}}

## Use cases
_Publications that use this measure:_

| Title                                                                                                          | Authors                    |
| -------------------------------------------------------------------------------------------------------------- | -------------------------- |
| [Challenges to established parties]({{< ref "usecases/abedi_challenges_2002.md" >}})                                       | Abedi (2002)               |
| [The Spatial Structure of Party Competition]({{< ref "usecases/andrews_spatial_2009.md" >}})                               | Andrews and Money (2009)   |
| [Elite polarization, party extremity, and affective polarization]({{< ref "usecases/banda_elite_2018.md" >}})              | Banda and Cluverius (2018) |
| [The impact of party polarisation and postmaterialism on voter turnout]({{< ref "usecases/crepaz_impact_1990.md" >}})      | Crepaz (1990)              |
| [Reflections]({{< ref "usecases/mair_reflections_1997.md" >}})                                                             | Mair and Castles (1997)    |
| [Electoral Rule Disproportionality and Platform Polarisation]({{< ref "usecases/matakos_electoral_2016.md" >}})            | Matakos et al. (2016)      |
| [Meaningful choices, political supply, and institutional effectiveness]({{< ref "usecases/wessels_meaningful_2008.md" >}}) | Wessels and Schmitt (2008) |

