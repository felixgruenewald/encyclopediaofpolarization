---
authors: Swen Hutter, Edgar Grande
date: 2014-01-01
title: Politicizing Europe in the National Electoral Arena
subtitle: A Comparative Analysis of Five West European Countries
publication: Journal of Common Market Studies
doi: https://doi.org/10.1111/jcms.12133
polarisation:
  - ideological
  - elite
measures: 
data: 
  - other
aliases:
  - Hutter and Grande (2014)
---

## Abstract
Although politicization has become a key concept in European integration studies, it is still contested whether, when and to what extent European issues have become politicized in domestic political arenas. This article contributes to this discussion both in conceptual and empirical terms. It uses a new multidimensional index of politicization to systematically trace the development of politicization in national election campaigns in five West European countries (Austria, Britain, France, Germany and Switzerland) from the 1970s to 2010. The findings provide clear evidence that Europe has indeed been politicized in the past decades. Moreover, two different paths towards such a politicization are identified. One of these paths is dominated by populist radical parties from the right, while the other path is shaped by the conflict between mainstream parties in government and opposition. On both paths, conflicts over membership play an important role and cultural-identitarian framing strategies are used.