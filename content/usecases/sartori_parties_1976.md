---
authors: Giovanni Sartori
date: 1976-01-01
title: Parties and party systems
subtitle: A framework for analysis
publication: Sage Publications
doi: 
polarisation:
  - ideological
  - elite
measures: 
data:
  - other
aliases:
  - Sartori (1976)
---

## Abstract
In this broad-ranging volume Sartori outlines a comprehensive and authoritative approach to the classification of party systems. He also offers an extensive review of the concept and rationale of the political party, and develops a sharp critique of various spatial models of party competition