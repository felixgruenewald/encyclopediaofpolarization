---
authors: Noam Gidron, James Adams, Will Horne
date: 2020-01-01
title: American Affective Polarization in Comparative Perspective
subtitle: 
publication: Elements in American Politics
doi: https://doi.org/10.1017/9781108914123
polarisation:
  - affective
  - elite
  - mass
  - vertical
measures:
  - dyads
data:
  - other
aliases:
  - Gidron et al. (2020)
---

## Abstract
American political observers express increasing concern about aﬀective polarization (i.e., partisans’ resentment toward political opponents). We advance debates about America’s partisan divisions by comparing aﬀective polarization in the USA over the past twenty-ﬁve years with aﬀective polarization in nineteen other Western publics. We conclude that American aﬀective polarization is not extreme in comparative perspective, although Americans’ dislike of partisan opponents has increased more rapidly since the mid 1990s than in most other Western publics. We then show that aﬀective polarization is more intense when unemployment and inequality are high; when political elites clash over cultural issues such as immigration and national identity; and in countries with majoritarian electoral institutions. Our ﬁndings situate American partisan resentment and hostility in comparative perspective and illuminate correlates of aﬀective polarization that are diﬃcult to detect when examining the American case in isolation.