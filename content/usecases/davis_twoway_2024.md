---
authors: B. Davis, J. Goodliffe, K. Hawkins
date: 2024-01-01
title: The Two-Way Effects of Populism on Affective Polarization
subtitle: 
publication: Comparative Political Studies
doi: https://doi.org/10.1177/00104140241237453
polarisation:
  - affective
  - mass
measures:
  - distance
  - variance-weighted
data:
  - cses
aliases:
  - Davis et al. (2024)
tags:
---

## Abstract

Despite attention in comparativist and Americanist literatures to populism and affective polarization, relatively little theoretical and empirical work has been done linking these two concepts. We present a comprehensive theory arguing that populism leads to greater affective polarization among both populist citizens and non-populist citizens, and that the latter effect grows as populism increases. We test this two-way effect using V-Dem expert rankings of populism and CSES surveys to measure affective polarization for 185 elections in 53 countries. This cross-regional analysis confirms and extends previous claims of a strong correlation between populist party identity and individual-level affective polarization; just as important, it also shows that an individual’s affective polarization is associated with populism at the country level, whether or not that individual is a supporter of populist parties. We show further that these results help explain a common finding in the comparative literature, that radical-right parties in Western democracies are disproportionately the target of animosity from other parties.