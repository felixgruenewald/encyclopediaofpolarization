---
authors: Hanspeter Kriesi, Edgar Grande, Romain Lachat, Martin Dolezal, Simon Bornschier, Timotheos Frey
date: 2006-01-01
title: Globalization and the transformation of the national political space
subtitle: Six European countries compared
publication: European Journal of Political Research
doi: https://doi.org/10.1111/j.1475-6765.2006.00644.x
polarisation:
  - ideological
  - elite
measures: 
data: 
  - other
aliases:
  - Kriesi et al. (2006)
---

## Abstract
This article starts from the assumption that the current process of globalization or denationalization leads to the formation of a new structural conflict in Western European countries, opposing those who benefit from this process against those who tend to lose in the course of the events. The structural opposition between globalization ‘winners’ and ‘losers’ is expected to constitute potentials for political mobilization within national political contexts, the mobilization of which is expected to give rise to two intimately related dynamics: the transformation of the basic structure of the national political space and the strategic repositioning of the political parties within the transforming space. The article presents several hypotheses with regard to these two dynamics and tests them empirically on the basis of new data concerning the supply side of electoral politics from six Western European countries (Austria, Britain, France, Germany, the Netherlands and Switzerland). The results indicate that in all the countries, the new cleavage has become embedded into existing twodimensional national political spaces, that the meaning of the original dimensions has been transformed, and that the configuration of the main parties has become triangular even in a country like France.