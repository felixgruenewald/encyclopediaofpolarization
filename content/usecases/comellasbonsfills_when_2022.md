---
authors: Josep Maria Comellas Bonsfills
date: 2022-01-01
title: When polarised feelings towards parties spread to voters
subtitle: The role of ideological distance and social sorting in Spain
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2022.102525
polarisation:
  - affective
  - elite
  - mass
  - vertical
  - horizontal
measures:
  - dyads
data:
  - other
aliases:
  - Comellas Bonsfills (2022)
---
## Abstract
Affective polarisation measured with feelings towards parties tends to overestimate the degree to which people dislike voters of opposing parties. This paper explores some of the factors that account for the gap between party affective polarisation (PAP) and voter affective polarisation (VAP). In particular, I first argue and show that the PAP-VAP gap increases with ideological distance between individuals and out-parties, although this difference begins to decrease after a certain level of ideological discrepancy is achieved. Second, social sorting increases the probability that individuals extend their antipathy towards parties to their voters, thus reducing the PAP-VAP gap. Third, whereas ideological distance leads to VAP among individuals with low levels of social sorting, it does not make a difference for socially sorted people. I discuss the relevance of these two factors by utilising the third wave of the E-DEM panel. The results have relevant implications for the consequences of affective polarisation.