---
authors: James Adams, David Bracken, Noam Gidron, Will Horne, Diana Z. O’brien, Kaitlin Senk
date: 2023-01-01
title: Can’t We All Just Get Along? How Women MPs Can Ameliorate Affective Polarization in Western Publics
subtitle: 
publication: American Political Science Review
doi: https://doi.org/10.1017/S0003055422000491
polarisation:
  - affective
  - mass
  - vertical
measures:
  - dyads
data:
  - cses
  - other
aliases:
  - Adams et al. (2023)
---

## Abstract
Concern over partisan resentment and hostility has increased across Western democracies. Despite growing attention to affective polarization, existing research fails to ask whether who serves in office affects mass-level interparty hostility. Drawing on scholarship on women’s behavior as elected representatives and citizens’ beliefs about women politicians, we posit the women MPs affective bonus hypothesis: all else being equal, partisans display warmer affect toward out-parties with higher proportions of women MPs. We evaluate this claim with an original dataset on women’s presence in 125 political parties in 20 Western democracies from 1996 to 2017 combined with survey data on partisans’ affective ratings of political opponents. We show that women’s representation is associated with lower levels of partisan hostility and that both men and women partisans react positively to out-party women MPs. Increasing women’s parliamentary presence could thus mitigate cross-party hostility.