---
authors: Lars Erik Berntzen, Haylee Kelsall, Eelco Harteveld
date: 2023-01-01
title: Consequences of affective polarization
subtitle: Avoidance, intolerance and support for violence in the United Kingdom and Norway
publication: European Journal of Political Research
doi: https://doi.org/10.1111/1475-6765.12623
polarisation:
  - affective
  - mass
  - horizontal
measures: 
data:
  - other
aliases:
  - Berntzen et al. (2023)
---

## Abstract
Affective polarization – that is, antipathy towards political opponents – sits high on the academic and political agenda. This is because it is thought to have a multitude of damaging consequences, both for how citizens view and approach each other and for how they relate to the political system. This study investigates some of the most mentioned and worrying potential consequences of affective polarization at the individual level. Zooming in on Europe, it sheds light on the substantive relationship between partisan antipathy and three kinds of norm-breaking escalation in the form of avoidance, intolerance and support for violence against party supporters. Methodologically, it unpacks the affective component of polarization, testing to what extent the traditional feeling thermometer performs as a predictor of these three potential outcomes. It then tests alternative expectations of the antecedents of such escalation derived from the intergroup emotions’ literature and the study of political radicalization. This is done using a broad range of both established and new survey items fielded in nationally representative panels between May and November 2020 in two contexts that score relatively low (Norway) and high (the United Kingdom) on affective polarization. They reveal that avoidance, intolerance and support for political violence can be validly measured, and are manifest, in these two European countries, but that they are only weakly correlated to mere dislike of the outgroup. Instead, more severe forms of norm-breaking escalation depend on the specific nature of the discrete emotions induced beyond dislike (anger, fear or disgust) and are rooted in factors such as relative deprivation, Manicheanism, and dark personality traits (psychopathy, Machiavellianism and narcissism). We discuss the implications for the way polarization is theorized and measured.