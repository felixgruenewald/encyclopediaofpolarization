---
authors: Zeev Maoz, Zeynep Somer-Topcu
date: 2010-01-01
title: Political Polarization and Cabinet Stability in Multiparty Systems
subtitle: A Social Networks Analysis of European Parliaments, 1945–98
publication: British Journal of Political Science
doi: https://doi.org/10.1017/S0007123410000220
polarisation:
  - ideological
  - elite
measures: 
data:
  - marpor
aliases:
  - Maoz and Sommer-Topcu (2010)
---

## Abstract
Bargaining theory predicts that as a political system’s polarization increases, parties have fewer opportunities to form coalitions without resorting to elections, inducing constraints on the management of political crises. This study tests the hypothesis that political polarization has a positive effect on cabinet duration, and draws on Social Networks Analysis to conceptualize and measure political polarization. Combining information about party ideology, inter-party distances and party size, this polarization index measures the structure of political systems in terms of possible and actual coalitions, and identifies proto-coalitions ex ante. The propositions regarding the effect of the bargaining environment on cabinet survival are tested with data covering sixteen European states in 1945–99, and are fairly robustly supported. The measure of political polarization outperforms alternative measures of this concept.