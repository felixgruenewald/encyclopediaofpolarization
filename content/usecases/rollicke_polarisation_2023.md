---
authors: Lena Röllicke
date: 2023-01-01
title: Polarisation, identity and affect
subtitle: Conceptualising affective polarisation in multi-party systems
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102655
polarisation:
  - affective
  - mass
  - elite
  - horizontal
  - vertical
measures:
data:
  - other
aliases:
  - Röllicke (2023)
---

## Abstract
This paper aims to contribute to the nascent field of research on affective polarisation in liberal democracies by reflecting on the conceptual ambiguities as well as potentials inherent in the concept. Based on a systematic, critical review of 78 articles, I discuss three main ambiguities in the current literature on affective polarisation in multi-party democracies. Those concern firstly, the object of dislike; secondly, the nature of dislike; and thirdly, how to make sense of the concept of “polarisation” in the context of affective polarisation. I then propose to use the existing ambiguities as a basis to work towards a more nuanced conceptualisation of affective polarisation which allows us to distinguish it from neighbouring concepts and to further differentiate between different constellations and degrees of affective polarisation. I conclude by arguing in favour of taking a broader approach to studying affective polarisation than done so far, and by suggesting some directions for future research.