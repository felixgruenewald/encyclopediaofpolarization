---
title: Horizontal Polarization
weight: 6
---

As [Druckman and Levendusky (2019)]({{< ref "druckman_what_2019" >}}) point out, orientations toward party elites and supporters are conceptually distinct – [Röllicke (2023)]({{< ref "rollicke_polarisation_2023" >}}) refers to measures based on the former as vertical polarization and those based on the latter as horizontal polarization – and may, therefore, also differ empirically. 

Initial findings from the Netherlands ([Harteveld 2021]({{< ref "harteveld_fragmented_2021" >}})), Israel ([Gidron et al. 2022]({{< ref "gidron_validating_2022" >}})), and Romania ([Ciobanu and Sandu 2022]({{< ref "ciobanu_what_2022" >}})) suggest that evaluations of parties and partisans are strongly but not perfectly correlated. Using survey data from Spain, [Comellas Bonsfills (2022)]({{< ref "comellasbonsfills_when_2022" >}}) shows that affective polarization measured by feelings toward parties tends to overestimate the extent to which people dislike voters of opposing parties but that the gap between party and partisan dislike decreases in the ideological distance between partisans. Finally, [Reiljan et al. (2023)]({{< ref "reiljan_patterns_2023" >}}), by computing measures of affective polarization from the [CSES’s]({{< ref "cses" >}}) like-dislike questions on parties and their leaders, show that both are strongly correlated but that affective polarization toward parties is stronger than toward their leaders. In contrast, by using distinct measures for out-party polarization (party thermometer) and out-partisan polarization (social distance measures), [Tichelbaecker et al. (2023)]({{< ref "tichelbaecker_what_2023" >}}) find only a modest relationship between both concepts.

{{< citation >}}

## Measures

| Measure                                               | Polarization                |
| ----------------------------------------------------- | --------------------------- |
| [Social Distance Scale]({{< ref "measures/socialdistance.md" >}}) | affective, mass, horizontal |


## Use cases

| Title                                                                                                                                          | Authors                        |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| [Elite communication and affective polarization among voters]({{< ref "usecases/bäck_elite_2023.md" >}})                                                   | Bäck et al. (2023)             |
| [Consequences of affective polarization]({{< ref "usecases/berntzen_consequences_2023.md" >}})                                                             | Berntzen et al. (2023)         |
| [The Politics of Interpersonal Trust and Reciprocity]({{< ref "usecases/carlin_politics_2013.md" >}})                                                      | Carlin and Love (2013)         |
| [What can De-Polarize the Polarizers?]({{< ref "usecases/ciobanu_what_2022.md" >}})                                                                        | Ciobanu and Sandu (2022)       |
| [Ideological identity, issue-based ideology and bipolar affective polarization in multiparty systems]({{< ref "usecases/comellas_ideological_2023.md" >}}) | Comellas and Torcal (2023)     |
| [When polarised feelings towards parties spread to voters]({{< ref "usecases/comellasbonsfills_when_2022.md" >}})                                          | Comellas Bonsfills (2022)      |
| [What Do We Measure When We Measure Affective Polarization?]({{< ref "usecases/druckman_what_2019.md" >}})                                                 | Druckman and Levendusky (2019) |
| [Does affective polarisation increase turnout?]({{< ref "usecases/harteveld_does_2023.md" >}})                                                             | Harteveld and Wagner (2023)    |
| [Fragmented foes]({{< ref "usecases/harteveld_fragmented_2021.md" >}})                                                                                     | Harteveld (2021)               |
| [Divided by the Vote]({{< ref "usecases/hobolt_divided_2020.md" >}})                                                                                       | Hobolt et al. (2020)           |
| [Affect, Not Ideology]({{< ref "usecases/iyengar_affect_2012.md" >}})                                                                                      | Iyengar et al. (2012)          |
| [Fear and Loathing across Party Lines]({{< ref "usecases/iyengar_fear_2015.md" >}})                                                                        | Iyengar and Westwood (2015)    |
| [Patterns of Affective Polarisation toward Parties and Leaders across the Democratic World]({{< ref "usecases/reiljan_patterns_2023.md" >}})               | Reiljan et al. (2023)          |
| [Polarisation, identity and affect]({{< ref "usecases/rollicke_polarisation_2023.md" >}})                                                                  | Röllicke (2023)                |
| [Intergroup contact reduces affective polarization but not among strong party identifiers]({{< ref "usecases/thomsen_intergroup_2023.md" >}})              | Thomsen and Thomsen (2023)     |
| [What Do We Measure When We Measure Affective Polarization across Countries?]({{< ref "usecases/tichelbaecker_what_2023.md" >}})                           | Tichelbaecker et al. (2023)    |
| [Social trust and affective polarization in Spain (2014–19)]({{< ref "usecases/torcal_social_2023.md" >}})                                                 | Torcal and Thomson (2023)      |
| [Affective polarization and coalition signals]({{< ref "usecases/wagner_affective_2023.md" >}})                                                            | Wagner and Praprotnik (2023)   |
| [Divided by the jab]({{< ref "usecases/wagner_divided_2024.md" >}})                                                                                        | Wagner and Eberl (2024)        |
| [The tie that divides]({{< ref "usecases/westwood_tie_2018.md" >}})                                                                                        | Westwood et al. (2018)         |

