---
title: Social Distance Scale
name: Closest Acceptable Relationship with Outparty Voters
polarisation:
  - affective
  - mass
  - horizontal
data: 
variables: 
usecases:
  - gidron_validating_2022
  - druckman_what_2019
  - iyengar_affect_2012
  - tichelbaecker_what_2023
  - wagner_affective_2023
math: false
---

## Description

Other than most of the other measures in this list, the Social Distance Scale does not compute an affective polarization score from other discrete items, but consists of one survey item that asks respondents of the closest relationship they would find acceptable with outparty voters. This way, it is supposed to measure not just like and dislike scores that are unclear in their implications, but the real societal impact of partisan differences.

[Gidron et al. (2022)]({{< ref "gidron_validating_2022" >}}) use a version of this scale with the response options ‘relative’, ‘close friend’, ‘neighbor’, ‘co-worker’, ‘citizen in my country’, ‘visitor to my country’, and ‘not at all’. 

Due to the complexity of this survey item, comparative data of this measurement is scarce.

## Use cases
_Publications that use this measure:_

| Title                                                                                                                             | Authors                        |
| --------------------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| [What Do We Measure When We Measure Affective Polarization?]({{< ref "usecases/druckman_what_2019.md" >}})                                    | Druckman and Levendusky (2019) |
| [Validating the feeling thermometer as a measure of partisan affect in multi-party systems]({{< ref "usecases/gidron_validating_2022.md" >}}) | Gidron et al. (2022)           |
| [Affect, Not Ideology]({{< ref "usecases/iyengar_affect_2012.md" >}})                                                                         | Iyengar et al. (2012)          |
| [What Do We Measure When We Measure Affective Polarization across Countries?]({{< ref "usecases/tichelbaecker_what_2023.md" >}})              | Tichelbaecker et al. (2023)    |
| [Affective polarization and coalition signals]({{< ref "usecases/wagner_affective_2023.md" >}})                                               | Wagner and Praprotnik (2023)   |
