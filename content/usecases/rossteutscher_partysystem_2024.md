---
authors: Sigrid Rossteutscher, Lars-Christopher Stövsand
date: 2024-01-01
title: Party-System Polarization and Individual Perceptions of Party Differences
subtitle: Two Divergent Effects on Turnout
publication: Government and Opposition
doi: https://doi.org/10.1017/gov.2023.43
polarisation:
  - ideological
  - elite
measures:
  - polarization-index
data:
  - cses
aliases:
  - Rossteutscher and Stövsand (2024)
---

## Abstract
In this article, we argue that party-system polarization and subjective perceptions of ideological party differences are conceptually and empirically distinct phenomena that affect electoral participation differently. Looking at 84 elections worldwide, we show that party-system polarization, and the sharp conflicts associated with it, depresses turnout because many citizens are put off by extreme party positions and unrewarding polemics. By contrast, the individual perception of differences between parties increases turnout because more citizens can find a party that is close to their own position and identify others as being further away. These opposite effects are possible because party-system polarization leads only some individuals to perceive differences between parties but leads others to avoid the emotionalized political arena. Moreover, individuals' ability to recognize differences between parties is not necessarily a consequence of party-system polarization. The contradictory findings in previous research are due to a conceptual and empirical blurring of these two essentially different aspects.