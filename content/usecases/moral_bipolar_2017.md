---
authors: Mert Moral
date: 2017-01-01
title: The Bipolar Voter
subtitle: On the Effects of Actual and Perceived Party Polarization on Voter Turnout in European Multiparty Democracies
publication: Political Behavior
doi: https://doi.org/10.1007/s11109-016-9386-0
polarisation:
  - ideological
  - mass
measures: 
data:
  - cses
aliases:
  - Moral (2017)
---

## Abstract
Despite the growing literature on polarization, students of comparative politics have not yet been able to reach much assured understanding of how party polarization influences voter turnout in multiparty settings, which often put on offer both centrist, and divergent mainstream and niche party policies. I evaluate how politically sophisticated and unsophisticated citizens with different ideological preferences respond to high and increasing party polarization by employing individual- and party system-level data from 17 European multiparty democracies. I hypothesize that high levels of actual and perceived party polarization increase voter turnout, and policy seeking, sophisticated citizens are more likely to turn out when polarization in party policy offerings in the short run increases their utility from voting. The empirical analyses show that high party polarization increases both politically sophisticated and unsophisticated citizens’ propensities to turn out. However, such positive effect for the most part comes from the between- and within-party systems differences in actual party polarization, rather than how individual citizens perceive that. The implications of these findings with respect to strategic position taking incentives of political parties and the effects of the knowledge gap between sophisticated and unsophisticated citizens on political participation and democratic representation are discussed in the concluding section.
