---
authors: Pedro Riera, Amuitz Garmendia Madariaga
date: 2023-01-01
title: Overlapping polarization
subtitle: On the contextual determinants of the interplay between ideological and affective polarization
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102628
polarisation:
  - ideological
  - affective
  - mass
  - vertical
measures:
  - spread
  - distance
data:
  - cses
aliases:
  - Riera and Madariaga (2023)
---

## Abstract
Previous literature has distinguished two types of polarization: ideological and affective. However, little is known on how the interconnection of these two polarizations (which we call overlapping polarization) varies depending on the political context. Is affective polarization always associated with ideological polarization? What is the role of the institutional framework (i.e., democratic age and popular election of the head of state) and the party system (i.e., elite polarization and number of parties) in determining how wide this overlap is? This article examines the contextual determinants of overlapping polarization by using information from the four first CSES waves. According to our analyses, the individual-level positive effect of ideological polarization on affective polarization is stronger when the party system is ideologically polarized and in older democracies, and is weaker in presidential democracies and when the number of parties is higher.