---
authors: Mariano Torcal, Zoe A. Thomson
date: 2023-01-01
title: Social trust and affective polarization in Spain (2014–19)
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102582
polarisation:
  - affective
  - mass
  - horizontal
measures:
  - distance
data:
  - other
aliases:
  - Torcal and Thomson (2023)
---

## Abstract
One overlooked aspect of the rising levels of affective polarization is its effect on general social trust. In the present article, we analyze the relationship between affective polarization and various measures of social trust using two separate panel surveys that were implemented in Spain in two different political periods (2014–2015 and 2018–2019), in order to assess whether the individual variations in the levels of affective polarization may affect people's trust in other members of society. Our findings suggest that affective polarization towards out-party members has a mutually reinforcing negative relationship with general social trust, generating a pervasive equilibrium of social and political deterioration that might contribute to worsening democratic quality. This effect is not compensated by any significant bonding effect resulting from in-partisan identification, whose effects seem to be restricted just to the very closest inner-circle individuals.