---
authors: Markus Wagner, Katrin Praprotnik
date: 2023-01-01
title: Affective polarization and coalition signals
subtitle: 
publication: Political Science Research and Methods
doi: https://doi.org/10.1017/psrm.2023.33
polarisation:
  - affective
  - mass
  - horizontal
measures:
  - socialdistance
data:
  - other
aliases:
  - Wagner and Praprotnik (2023)
---

## Abstract
Affective polarization between partisans is potentially troubling for liberal democracy. Hence, recent research has focused on how affective dislike between partisans can be reduced. Using a survey experiment in Austria, we test whether elite signals matter. Respondents exposed to fictional news stories implying that their in-party might form a coalition with an out-party show reduced dislike toward supporters of that out-party. Our experiment also shows that coalition signals can influence out-party affect even if neither of the two parties signaling cooperation are an in-party. We conclude that cooperation between rivals has an important role in reducing affective polarization.