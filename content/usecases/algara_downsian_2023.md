---
authors: Carlos Algara, Roi Zur
date: 2023-01-01
title: The Downsian roots of affective polarization
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102581
polarisation:
  - affective
  - ideological
  - mass
measures: 
data:
  - other
aliases:
  - Algara and Zur (2023)
---

## Abstract

A growing literature studies the relationship between ideological and affective polarization. By taking a Downsian approach to affective polarization we contribute to this literature and demonstrating that affective polarization is driven by congruence between citizens and their party, relative to other parties, in the general liberal-conservative space and across a host of salient issue domains. We find robust support for our theory using individual-level national election survey data from the United States, United Kingdom, Germany, and Finland. Moreover, we find that ideological differences inform affective polarization independently from partisan identifications and that they drive more out-party animosity than in-party affinity. These findings have implications towards a more unified understanding of the citizen determinants of affective polarization and the role ideology plays in shaping the views held by partisans across democracies.