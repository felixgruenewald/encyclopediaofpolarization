---
authors: Patrik Öhberg, Felix Cassel
date: 2023-01-01
title: Election campaigns and the cyclical nature of emotions—How politicians engage in affective polarization
subtitle: 
publication: Scandinavian Political Studies
doi: https://doi.org/10.1111/1467-9477.12258
polarisation:
  - affective
  - elite
measures: 
data:
  - other
aliases:
  - Öhberg and Cassel (2023)
---

## Abstract
Abstract Do politicians get emotional during an election campaign? We examine the existence of changes in partisan in‐group favoritism and partisan out‐group hostility among political elites by evaluating the degree to which they fluctuate before, during and after election campaigns. The lack of elite level panel data has prevented scholars from studying the dynamics of politicians' emotions around the most emotionally intense political event in democracies: elections. We focus on Sweden around the 2014 election and follow more than 700 Swedish politicians before, during and after a national election campaign using a unique three‐wave panel survey. The results reveal that politicians' emotions towards other parties are affected during the election, but less so for their own party. Our study adds to the body of recent evidence that campaigns mobilize partisan identities and increase partisan animus.