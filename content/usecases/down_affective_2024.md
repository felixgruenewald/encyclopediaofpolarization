---
authors: I. Down, K.-J. Han
date: 2024-01-01
title: Affective polarization and strategic voting in Britain
subtitle:
publication: Journal of Elections, Public Opinion and Parties
doi: https://doi.org/10.1080/17457289.2024.2337422
polarisation:
  - affective
  - vertical
  - elite
measures:
  - distance
data:
  - bes
aliases:
  - Down and Han (2024)
tags:
---

## Abstract
Do affective attitudes determine voting behavior? As a part of this broad question, we examine how third-party supporters’ affective attitudes decide their strategic voting behavior. Using seven waves of the British Election Study (2015, 2017, and 2019), we find that third-party supporters whose affective attitudes toward the two principal political parties in their constituency are more polarized are more inclined to vote strategically. The results imply that voting behavior is shaped by both feelings toward as well as cognitive evaluations of political parties. They also imply that British voters may increasingly engage in strategic voting if the level of affective polarization continues to rise. Lastly, and more generally, the results imply that all political parties should pay attention to voter “feelings” if they are to maximize their electoral support.