---
title: Ideological Polarization
weight: 1
---

Beginning with [Sartori (1976)]({{< ref "sartori_parties_1976" >}})'s seminal work on party systems, polarization, which he defined as growing distance between political actors, has been a central theme in the literature on multiparty systems in Europe and beyond ([Dassonneville and Çakır 2021]({{< ref "dassonneville_party_2021" >}})). [Sartori (1976)]({{< ref "sartori_parties_1976" >}}), and most of the literature until recently, understood distance in a purely programmatic sense, often along a single left-right dimension. In its simplest form, ideological polarization can be measured as the distance between the policy positions of parties or their supporters, but more sophisticated methods also take into account the full distribution of opinions across the political spectrum.

Polarization is different from party-system fractionalization ([Dalton 2008]({{< ref "dalton_quantity_2008" >}})), which measures the distribution of vote or seat shares across parties, but can be considered a component of politicization ([Hutter and Grande 2014]({{< ref "hutter_politicizing_2014" >}})). Although conceptually distinct, polarization tends to correlate with party-system fractionalization because many measures, as we will see in the next section, integrate vote and seat shares. In the literature on the politicization of the EU, politicization is defined as the extent to which the EU is both a salient and politically contested, i.e., polarized, political issue.

{{< citation >}}

## Measures

| Measure                                                        | Polarization                           |
| -------------------------------------------------------------- | -------------------------------------- |
| [Party-System Compactness]({{< ref "measures/compactness.md" >}})          | ideological, elite                     |
| [Dispersion]({{< ref "measures/dispersion.md" >}})                         | ideological, elite                     |
| [Party Dyads]({{< ref "measures/dyads.md" >}})                             | ideological, affective, mass, vertical |
| [Party-System Extremism]({{< ref "measures/party-system_extremism.md" >}}) | ideological, elite                     |
| [Polarization Index]({{< ref "measures/polarization-index.md" >}})         | ideological, elite, mass               |
| [Range]({{< ref "measures/range.md" >}})                                   | ideological, elite                     |
| [SD]({{< ref "measures/sd.md" >}})                                         | ideological, elite, mass               |
| [Variance]({{< ref "measures/variance-weighted.md" >}})                    | ideological, elite                     |

## Use cases
_Publications that address ideological polarization:_

| Title                                                                                                                                                      | Authors                           |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------- |
| [Challenges to established parties]({{< ref "usecases/abedi_challenges_2002.md" >}})                                                                                   | Abedi (2002)                      |
| [Is Polarization a Myth?]({{< ref "usecases/abramowitz_polarization_2008.md" >}})                                                                                      | Abramowitz and Saunders (2008)    |
| [The Downsian roots of affective polarization]({{< ref "usecases/algara_downsian_2023.md" >}})                                                                         | Algara and Zur (2023)             |
| [Party System Compactness]({{< ref "usecases/alvarez_party_2004.md" >}})                                                                                               | Alvarez and Nagler (2004)         |
| [The Activists Who Divide Us]({{< ref "usecases/amitai_activists_2023.md" >}})                                                                                         | Amitai (2023)                     |
| [The Spatial Structure of Party Competition]({{< ref "usecases/andrews_spatial_2009.md" >}})                                                                           | Andrews and Money (2009)          |
| [Elite polarization, party extremity, and affective polarization]({{< ref "usecases/banda_elite_2018.md" >}})                                                          | Banda and Cluverius (2018)        |
| [Has Trust in the European Parliament Polarized?]({{< ref "usecases/bauer_has_2023.md" >}})                                                                            | Bauer and Morisi (2023)           |
| [Elite polarisation and voting turnout in Latin America]({{< ref "usecases/bejar_elite_2020.md" >}})                                                                   | Béjar et al. (2020)               |
| [Institutional design and polarization]({{< ref "usecases/bernaerts_institutional_2023.md" >}})                                                                        | Bernaerts et al. (2023)           |
| [Political polarisation and environmental attitudes]({{< ref "usecases/birch_political_2020.md" >}})                                                                   | Birch (2020)                      |
| [Do Voters Polarize When Radical Parties Enter Parliament?]({{< ref "usecases/bischof_voters_2019.md" >}})                                                             | Bischof and Wagner (2019)         |
| [Cleavage politics, polarisation and participation in Western Europe]({{< ref "usecases/borbath_cleavage_2023.md" >}})                                                 | Borbáth et al. (2023)             |
| [Understanding Polarization]({{< ref "usecases/bramson_understanding_2017.md" >}})                                                                                     | Bramson et al. (2017)             |
| [Polarization and ideological congruence between parties and supporters in Europe]({{< ref "usecases/carroll_polarization_2018.md" >}})                                | Caroll and Kubo (2018)            |
| [The impact of party polarisation and postmaterialism on voter turnout]({{< ref "usecases/crepaz_impact_1990.md" >}})                                                  | Crepaz (1990)                     |
| [Missing Links in Party-System Polarisation]({{< ref "usecases/curini_missing_2012.md" >}})                                                                            | Curini and Hino (2012)            |
| [Ideological Polarization and Far-Right Parties in Europe]({{< ref "usecases/dalton_ideological_2022.md" >}})                                                          | Dalton and Berning (2022)         |
| [Modeling ideological polarisation in democratic party systems]({{< ref "usecases/dalton_modeling_2021.md" >}})                                                        | Dalton (2021)                     |
| [The Quantity and the Quality of Party Systems]({{< ref "usecases/dalton_quantity_2008.md" >}})                                                                        | Dalton (2008)                     |
| [Party System Polarisation and Electoral Behavior]({{< ref "usecases/dassonneville_party_2021.md" >}})                                                                 | Dassonneville and Çakır (2021)    |
| [Party-System Extremism in Majoritarian and Proportional Electoral Systems]({{< ref "usecases/dow_partysystem_2011.md" >}})                                            | Dow (2011)                        |
| [Does voter polarisation induce party extremism?]({{< ref "usecases/dreyer_does_2019.md" >}})                                                                          | Dreyer and Bauer (2019)           |
| [Parties` Policy Programmes and the Dog that Didn't Bark]({{< ref "usecases/ezrow_parties_2008.md" >}})                                                                | Ezrow (2008)                      |
| [The Variance Matters]({{< ref "usecases/ezrow_variance_2007.md" >}})                                                                                                  | Ezrow (2007)                      |
| [Centre Parties]({{< ref "usecases/hazan_centre_1997.md" >}})                                                                                                          | Hazan (1997)                      |
| [The mobilising effect of political choice]({{< ref "usecases/hobolt_mobilising_2019.md" >}})                                                                          | Hobolt and Hoerner (2019)         |
| [Does Left/Right Structure Party Positions on European Integration?]({{< ref "usecases/hooghe_does_2002.md" >}})                                                       | Hooghe et al. (2002)              |
| [Politicizing Europe in the National Electoral Arena]({{< ref "usecases/hutter_politicizing_2014.md" >}})                                                              | Hutter and Grande (2014)          |
| [Does Austerity Cause Polarization?]({{< ref "usecases/hübscher_does_2023.md" >}})                                                                                     | Hübscher et al. (2023)            |
| [Sweet victory, bitter defeat]({{< ref "usecases/janssen_sweet_2023.md" >}})                                                                                           | Janssen (2023)                    |
| [The Left-Right Semantics and the New Politics Cleavage]({{< ref "usecases/kitschelt_leftright_1990.md" >}})                                                           | Kitschelt and Hellemans (1990)    |
| [Globalization and the transformation of the national political space]({{< ref "usecases/kriesi_globalization_2006.md" >}})                                            | Kriesi et al. (2006)              |
| [Party Polarisation and Mass Partisanship]({{< ref "usecases/lupu_party_2015.md" >}})                                                                                  | Lupu (2015)                       |
| [Reflections]({{< ref "usecases/mair_reflections_1997.md" >}})                                                                                                         | Mair and Castles (1997)           |
| [Political Polarization and Cabinet Stability in Multiparty Systems]({{< ref "usecases/maoz_political_2010.md" >}})                                                    | Maoz and Sommer-Topcu (2010)      |
| [Electoral Rule Disproportionality and Platform Polarisation]({{< ref "usecases/matakos_electoral_2016.md" >}})                                                        | Matakos et al. (2016)             |
| [The Bipolar Voter]({{< ref "usecases/moral_bipolar_2017.md" >}})                                                                                                      | Moral (2017)                      |
| [On the relationship between party polarisation and citizen polarisation]({{< ref "usecases/moral_relationship_2023.md" >}})                                           | Moral and Best (2023)             |
| [Measuring partisan polarization with partisan differences in satisfaction with the government]({{< ref "usecases/patkós_measuring_2023.md" >}})                       | Patkós (2023)                     |
| [Polarization and correct voting in U.S. presidential elections]({{< ref "usecases/pierce_polarization_2019.md" >}})                                                   | Pierce and Lau (2019)             |
| [Overlapping polarization]({{< ref "usecases/riera_overlapping_2023.md" >}})                                                                                           | Riera and Madariaga (2023)        |
| [Party-System Polarization and Individual Perceptions of Party Differences]({{< ref "usecases/rossteutscher_partysystem_2024.md" >}})                                  | Rossteutscher and Stövsand (2024) |
| [Parties and party systems]({{< ref "usecases/sartori_parties_1976.md" >}})                                                                                            | Sartori (1976)                    |
| [Left-Right Polarisation In National Party Systems]({{< ref "usecases/sigelman_leftright_1978.md" >}})                                                                 | Sigelman and Yough (1978)         |
| [Economic Integration, Party Polarisation and Electoral Turnout]({{< ref "usecases/steiner_economic_2012.md" >}})                                                      | Steiner and Martin (2012)         |
| [Party Systems and Government Stability]({{< ref "usecases/taylor_party_1971.md" >}})                                                                                  | Taylor and Herman (1971)          |
| [Group-based public opinion polarisation in multi-party systems]({{< ref "usecases/traber_groupbased_2023.md" >}})                                                     | Traber et al. (2023)              |
| [Conflict or choice? The differential effects of elite incivility and ideological polarization on political support]({{< ref "usecases/vanelsas_conflict_2023.md" >}}) | van Elsas and Fiselier (2023)     |
| [From political conflict to partisan evaluations]({{< ref "usecases/vegetti_political_2014.md" >}})                                                                    | Vegetti (2014)                    |
| [Meaningful choices, political supply, and institutional effectiveness]({{< ref "usecases/wessels_meaningful_2008.md" >}})                                             | Wessels and Schmitt (2008)        |
| [Polarization, Number of Parties, and Voter Turnout]({{< ref "usecases/wilford_polarization_2017.md" >}})                                                              | Wilford (2017)                    |

