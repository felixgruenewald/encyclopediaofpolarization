---
authors: Markus Wagner, Jakob-Moritz Eberl
date: 2024-01-01
title: Divided by the jab
subtitle: Affective polarisation based on COVID vaccination status
publication: Journal of Elections, Public Opinion and Parties
doi: https://doi.org/10.1080/17457289.2024.2352449
polarisation:
  - affective
  - mass
  - horizontal
measures: 
data:
  - other
aliases:
  - Wagner and Eberl (2024)
---


## Abstract
Group-based affective polarisation can emerge around new issues that divide citizens. The public response to vaccines against COVID-19 provided a clear example of a new basis for group divides. Despite scientific consensus regarding the dangers of SARS-CoV-2 as well as the safety and effectiveness of available vaccinations, the public response to the COVID-19 pandemic was strongly politicised during the height of the health crisis. Positive social identities and negative out-group stereotyping developed around support or opposition to the vaccines. Panel survey data from Austria shows that vaccination identities are clearly identifiable and are related to extensive traitbased stereotyping of in- and out-group members. Moreover, we show that vaccination identities are linked to political identities and orientations that pre-date the politicisation of COVID-19 vaccines. Indeed, vaccination identities are more strongly related to political orientations than the decision to get vaccinated itself. Importantly, vaccination identities help us understand downstream attitudes, preferences, and behaviours related to the pandemic, even when controlling for other important predictors such as vaccination status and partisanship for anti-vaccine parties. We discuss the implications and generalizability of our findings beyond the context of the pandemic.