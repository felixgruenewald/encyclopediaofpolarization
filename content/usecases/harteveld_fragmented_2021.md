---
authors: Eelco Harteveld
date: 2021-01-01
title: Fragmented foes
subtitle: Affective polarization in the multiparty context of the Netherlands
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2021.102332
polarisation:
  - affective
  - elite
  - mass
  - horizontal
  - vertical
measures:
  - api
data:
  - cses
aliases:
  - Harteveld (2021)
---

## Abstract
Affective polarization, or antipathy between the supporters of opposing political camps, is documented to be on the rise in the United States and elsewhere. At the same time, there are limits to our understanding of this phenomenon in multiparty contexts. How do citizens draw the line between 'ingroups' and 'outgroups' in fragmented contexts with multiple parties? Answering this question has been hampered by a relative lack of data on citizens' views towards compatriots with opposing political views outside the US. This study is based on original data collection, measuring citizens’ evaluations of various political and non-political outgroups among a population-representative sample of 1071 Dutch citizens. These data allow to study the extent and configuration of affective polarization in the highly fragmented context of the Netherlands. The analysis shows that respondents do distinguish between parties and partisans. They report more dislike towards political outgroups than towards almost all non-political outgroups. Rather than disliking all out-partisans equally, evaluations grow gradually colder as ideological distance towards a group increases. Affective polarization is especially strong between those who disagree on cultural issues, and between those who support and oppose the populist radical right. The article discusses how these findings enhance our understanding of affective polarization in multiparty systems.