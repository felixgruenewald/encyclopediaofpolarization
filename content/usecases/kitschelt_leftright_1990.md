---
authors: Herbert Kitschelt, Staf Hellemans
date: 1990-01-01
title: The Left-Right Semantics and the New Politics Cleavage
subtitle: 
publication: Comparative Political Studies
doi: https://doi.org/10.1177/0010414090023002003
polarisation:
  - ideological
measures: 
data: 
  - other
aliases:
  - Kitschelt and Hellemans (1990)
---

## Abstract
Since the 1960s, political scientists have debated the continued relevance of the left-right vocabulary for structuring policy choices and party affiliation in the mass publics of modern democracies. With the rise of “new politics” and “left-libertarian” movements and parties that try to redefine the political agenda of advanced democracies this issue has gained additional interest. In this article we first present four theories about the decline, persistence, transformation, or pluralization of the meaning new politics activists give to the left-right language. Then we explore how new politics activists in the Belgian ecology parties Agalev and Ecolo construct the meaning of _left_ and _right._ For ecology party militants, this terminology still has an economic meaning, yet also gains a cultural significance that relates to the choice between a modern, highly centralized, and differentiated society and efforts to create a postmodern, decentralized, and more communitarian social order. Thus our data support the argument of pluralization theory that the meaning of left and right becomes multidimensional.