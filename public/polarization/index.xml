<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Definitions of Polarization on Encyclopedia of Polarization</title>
    <link>//localhost:1313/polarization/</link>
    <description>Recent content in Definitions of Polarization on Encyclopedia of Polarization</description>
    <generator>Hugo</generator>
    <language>en-us</language>
    <atom:link href="//localhost:1313/polarization/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Ideological Polarization</title>
      <link>//localhost:1313/polarization/ideological/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/polarization/ideological/</guid>
      <description>&lt;p&gt;Beginning with &lt;a href=&#34;//localhost:1313/usecases/sartori_parties_1976/&#34; &gt;Sartori (1976)&lt;/a&gt;&amp;rsquo;s seminal work on party systems, polarization, which he defined as growing distance between political actors, has been a central theme in the literature on multiparty systems in Europe and beyond (&lt;a href=&#34;//localhost:1313/usecases/dassonneville_party_2021/&#34; &gt;Dassonneville and Çakır 2021&lt;/a&gt;). &lt;a href=&#34;//localhost:1313/usecases/sartori_parties_1976/&#34; &gt;Sartori (1976)&lt;/a&gt;, and most of the literature until recently, understood distance in a purely programmatic sense, often along a single left-right dimension. In its simplest form, ideological polarization can be measured as the distance between the policy positions of parties or their supporters, but more sophisticated methods also take into account the full distribution of opinions across the political spectrum.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Affective Polarization</title>
      <link>//localhost:1313/polarization/affective/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/polarization/affective/</guid>
      <description>&lt;p&gt;Recently, an emerging literature, pioneered in the US context by &lt;a href=&#34;//localhost:1313/usecases/iyengar_affect_2012/&#34; &gt;Iyengar et al. (2012)&lt;/a&gt;, has begun to distinguish affective from programmatic polarization. The phenomenon of affective polarization takes its roots in differing political positions of political actors. However, affective polarization goes beyond the programmatic side of politics and highlights that political identities go along with sympathies for members of one’s own political camp and antipathies toward the opposing side. Affective polarization is most commonly understood as the difference between ingroup like and outgroup dislike, although this is not the only possible way to define it (see &lt;a href=&#34;//localhost:1313/usecases/rollicke_polarisation_2023/&#34; &gt;Röllicke 2023&lt;/a&gt;).&lt;/p&gt;</description>
    </item>
    <item>
      <title>Elite Polarization</title>
      <link>//localhost:1313/polarization/elite/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/polarization/elite/</guid>
      <description>&lt;p&gt;Ideological polarization on the elite level can be measured in its simplest form as the absolute ideological distance between the positions of the most extreme party on the left and the most extreme party on the right of the political spectrum.&lt;/p&gt;&#xA;&lt;p&gt;A number of studies have examined whether ideological polarization among parties translates into affective polarization among the mass public (&lt;a href=&#34;//localhost:1313/usecases/banda_elite_2018/&#34; &gt;Banda and Cluverius (2018)&lt;/a&gt;). However, as these studies were conducted in the dichotomous US party system, the question remains to what extent these findings generalize to multiparty systems.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Mass Polarization</title>
      <link>//localhost:1313/polarization/mass/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/polarization/mass/</guid>
      <description>&lt;p&gt;A large number of measures can be classified as measures of mass polarization, i.e., polarization that is measured between members of the broader public. This is to a great degree due to the fact that the most commonly available datasets are general population surveys. However, one should still distinct whether the measures are directed at other members of the public (&lt;a href=&#34;//localhost:1313/polarization/horizontal/&#34; &gt;horizontal polarization&lt;/a&gt;) or at political elites (&lt;a href=&#34;//localhost:1313/polarization/vertical/&#34; &gt;vertical polarization&lt;/a&gt;). One could, for example, ask for opinions of the voters of a certain party, or of the party and its officials itself.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Vertical Polarization</title>
      <link>//localhost:1313/polarization/vertical/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/polarization/vertical/</guid>
      <description>&lt;p&gt;As &lt;a href=&#34;//localhost:1313/usecases/druckman_what_2019/&#34; &gt;Druckman and Levendusky (2019)&lt;/a&gt; point out, orientations toward party elites and supporters are conceptually distinct – &lt;a href=&#34;//localhost:1313/usecases/rollicke_polarisation_2023/&#34; &gt;Röllicke (2023)&lt;/a&gt; refers to measures based on the former as vertical polarization and those based on the latter as horizontal polarization – and may, therefore, also differ empirically.&lt;/p&gt;&#xA;&lt;p&gt;Initial findings from the Netherlands (&lt;a href=&#34;//localhost:1313/usecases/harteveld_fragmented_2021/&#34; &gt;Harteveld 2021&lt;/a&gt;), Israel (&lt;a href=&#34;//localhost:1313/usecases/gidron_validating_2022/&#34; &gt;Gidron et al. 2022&lt;/a&gt;), and Romania (&lt;a href=&#34;//localhost:1313/usecases/ciobanu_what_2022/&#34; &gt;Ciobanu and Sandu 2022&lt;/a&gt;) suggest that evaluations of parties and partisans are strongly but not perfectly correlated. Using survey data from Spain, &lt;a href=&#34;//localhost:1313/usecases/comellasbonsfills_when_2022/&#34; &gt;Comellas Bonsfills (2022)&lt;/a&gt; shows that affective polarization measured by feelings toward parties tends to overestimate the extent to which people dislike voters of opposing parties but that the gap between party and partisan dislike decreases in the ideological distance between partisans. Finally, &lt;a href=&#34;//localhost:1313/usecases/reiljan_patterns_2023/&#34; &gt;Reiljan et al. (2023)&lt;/a&gt;, by computing measures of affective polarization from the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES’s&lt;/a&gt; like-dislike questions on parties and their leaders, show that both are strongly correlated but that affective polarization toward parties is stronger than toward their leaders. In contrast, by using distinct measures for out-party polarization (party thermometer) and out-partisan polarization (social distance measures), &lt;a href=&#34;//localhost:1313/usecases/tichelbaecker_what_2023/&#34; &gt;Tichelbaecker et al. (2023)&lt;/a&gt; find only a modest relationship between both concepts.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Horizontal Polarization</title>
      <link>//localhost:1313/polarization/horizontal/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/polarization/horizontal/</guid>
      <description>&lt;p&gt;As &lt;a href=&#34;//localhost:1313/usecases/druckman_what_2019/&#34; &gt;Druckman and Levendusky (2019)&lt;/a&gt; point out, orientations toward party elites and supporters are conceptually distinct – &lt;a href=&#34;//localhost:1313/usecases/rollicke_polarisation_2023/&#34; &gt;Röllicke (2023)&lt;/a&gt; refers to measures based on the former as vertical polarization and those based on the latter as horizontal polarization – and may, therefore, also differ empirically.&lt;/p&gt;&#xA;&lt;p&gt;Initial findings from the Netherlands (&lt;a href=&#34;//localhost:1313/usecases/harteveld_fragmented_2021/&#34; &gt;Harteveld 2021&lt;/a&gt;), Israel (&lt;a href=&#34;//localhost:1313/usecases/gidron_validating_2022/&#34; &gt;Gidron et al. 2022&lt;/a&gt;), and Romania (&lt;a href=&#34;//localhost:1313/usecases/ciobanu_what_2022/&#34; &gt;Ciobanu and Sandu 2022&lt;/a&gt;) suggest that evaluations of parties and partisans are strongly but not perfectly correlated. Using survey data from Spain, &lt;a href=&#34;//localhost:1313/usecases/comellasbonsfills_when_2022/&#34; &gt;Comellas Bonsfills (2022)&lt;/a&gt; shows that affective polarization measured by feelings toward parties tends to overestimate the extent to which people dislike voters of opposing parties but that the gap between party and partisan dislike decreases in the ideological distance between partisans. Finally, &lt;a href=&#34;//localhost:1313/usecases/reiljan_patterns_2023/&#34; &gt;Reiljan et al. (2023)&lt;/a&gt;, by computing measures of affective polarization from the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES’s&lt;/a&gt; like-dislike questions on parties and their leaders, show that both are strongly correlated but that affective polarization toward parties is stronger than toward their leaders. In contrast, by using distinct measures for out-party polarization (party thermometer) and out-partisan polarization (social distance measures), &lt;a href=&#34;//localhost:1313/usecases/tichelbaecker_what_2023/&#34; &gt;Tichelbaecker et al. (2023)&lt;/a&gt; find only a modest relationship between both concepts.&lt;/p&gt;</description>
    </item>
  </channel>
</rss>
