---
authors: Alexander Ryan
date: 2023-01-01
title: Exploring differences in affective polarization between the Nordic countries
subtitle: 
publication: Scandinavian Political Studies
doi: https://doi.org/10.1111/1467-9477.12244
polarisation:
  - affective
  - mass
  - vertical
measures:
  - spread
  - distance
data:
  - cses
aliases:
  - Ryan (2023)
---

## Abstract
Abstract After a period in which affective polarization—defined here as the difference between positive feelings toward in‐parties and negative out‐party animus—has mostly focused on the single US case, there has recently been an increase in large‐N comparative analyses and single case studies in other countries, including in the Nordic region. This study adds to this by studying and comparing affective polarization in the Nordic countries. In line with what previous comparative and single case studies have already indicated, the results show that affective polarization has tended to be higher in Sweden and Denmark than in Norway, Iceland, and Finland. The article also tracks time trends for the association between ideological distance from parties and affective party evaluations. As expected, placing parties further from oneself on the left‐right scale has been more strongly associated with party affect in Denmark and Sweden. Furthermore, the results show that there are some variations between the countries in terms of how distance from parties on other ideological dimensions than left‐right placement is associated with out‐party affect.