---
authors: Ivo Bantel
date: 2023-01-01
title: Camps, not just parties
subtitle: The dynamic foundations of affective polarization in multi-party systems
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102614
polarisation:
  - affective
  - elite
  - vertical
measures:
  - dyads
data:
  - cses
aliases:
  - Bantel (2023)
---

## Abstract
Concerns over affective polarization in Western democracies are growing. But which broader political distinctions are also affective demarcations? As inter-party cooperation is the rule in multi-party democracies, explaining affective polarization beyond partisan divisions is crucial. I argue that demarcations between political camps deepen affective polarization, and country-level factors influence the relevance of these affective divides. Based on survey data from 23 Western democracies (1996–2019), I demonstrate that affect is most polarized between Left and Right camps, and between the Radical Right and other camps. Further, these divides are dynamic and depend on different country-level outcomes. The Left/Right divide disappears when Left and Right parties govern together, while the Radical Right divide is fortified with Radical Right electoral success. These findings highlight that affective polarization’s group foundations extend beyond partisanship, and that affective polarization could even act as a defence mechanism against radical challengers.