<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Encyclopedia of Polarization</title>
    <link>//localhost:1313/measures/</link>
    <description>Recent content on Encyclopedia of Polarization</description>
    <generator>Hugo</generator>
    <language>en-us</language>
    <atom:link href="//localhost:1313/measures/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>API</title>
      <link>//localhost:1313/measures/api/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/api/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;The Affective Polarization Index (API) was developed by &lt;a href=&#34;//localhost:1313/usecases/reiljan_fear_2020/&#34; &gt;Reiljan (2020)&lt;/a&gt;. It is based on Dalton&amp;rsquo;s Polarization Index and measures the (weighted) average difference between partisans&amp;rsquo; like of their party, including leaners, and their dislike of all other parties. Reiljan computes the API based on the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt; dataset, but in principle it can be applied to other datasets that contain the required items, i.e., like-dislike scores and partisan identification questions. For example, &lt;a href=&#34;//localhost:1313/usecases/garzia_affective_2023/&#34; &gt;Garzia et al. (2023)&lt;/a&gt; applies the measure to The West European Voter Dataset and national election studies from Australia, Canada, New Zealand, and the United States.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Dispersion</title>
      <link>//localhost:1313/measures/dispersion/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/dispersion/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;&lt;a href=&#34;//localhost:1313/usecases/ezrow_variance_2007/&#34; &gt;Ezrow (2007)&lt;/a&gt; proposed the (Weighted) Party System Dispersion measure, which is basically a weighted standard deviation formula. Both &lt;a href=&#34;//localhost:1313/usecases/ezrow_variance_2007/&#34; &gt;Ezrow (2007)&lt;/a&gt; and &lt;a href=&#34;//localhost:1313/usecases/dreyer_does_2019/&#34; &gt;Dreyer and Bauer (2019)&lt;/a&gt; use &lt;a href=&#34;//localhost:1313/data/marpor/&#34; &gt;MARPOR&lt;/a&gt; data to measure party positions, but in principle it can be used with other datasets that contain the required information, i.e. party positions.&lt;/p&gt;&#xA;&lt;h2 id=&#34;operationalization&#34;&gt;Operationalization&lt;/h2&gt;&#xA;&lt;p&gt;The (weighted) party system dispersion is measured as follows:&lt;/p&gt;&#xA;&lt;p&gt;$Weighted&lt;del&gt;party&lt;/del&gt;system~dispersion = \sqrt{\sum_{j=1} VS_{j} (P_{jk} - \bar{P}_k)^2}$&lt;/p&gt;&#xA;&lt;p&gt;where $P_{jk}$ is the ideological position of party $j$ in country $k$ and $\bar{P_k}$ is the weighted average of the left-right ideological positions of all parties in country year $k$. $VS_j$ is the vote share of party $j$ in the last national election.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Distance</title>
      <link>//localhost:1313/measures/distance/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/distance/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;The (weighted) mean distance from the most liked party is a measure suggested by &lt;a href=&#34;//localhost:1313/usecases/wagner_affective_2021/&#34; &gt;Wagner (2021)&lt;/a&gt;, although he considers it inferior to the (weighted) spread. It measures the average discrepancy between like for the most liked party and like/dislike for all other parties. Wagner computes the (weighted) distance based on the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt; dataset, but in principle it can be applied to other datasets that contain the required items, i.e., like-dislike scores.&#xA;​&lt;/p&gt;</description>
    </item>
    <item>
      <title>Party Dyads</title>
      <link>//localhost:1313/measures/dyads/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/dyads/</guid>
      <description>&lt;p&gt;In political systems with just two parties, polarization describes the distance between these two poles. Therefore, much of the classical American polarization literature, ideological or affective, relied on the difference between a respondent&amp;rsquo;s inparty and outparty, either based on like-dislike scores or the perception of the parties&amp;rsquo; positions.&lt;/p&gt;&#xA;&lt;p&gt;Party dyads can be seen as a related form of measurement in multiparty systems, where the difference between two selected parties is taken as an indication of general polarization. This can be done with just one or with multiple dyads. It describes a very tangible form of polarization that can be used to show how relationships to individual parties are affected (&lt;a href=&#34;//localhost:1313/usecases/bantel_camps_2023/&#34; &gt;Bantel (2023)&lt;/a&gt;) or to compare polarization levels between two- and multiparty systems (&lt;a href=&#34;//localhost:1313/usecases/gidron_american_2020/&#34; &gt;Gidron et al. (2020)&lt;/a&gt;).&lt;/p&gt;</description>
    </item>
    <item>
      <title>Party-System Compactness</title>
      <link>//localhost:1313/measures/compactness/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/compactness/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;Party-system compactness, proposed by &lt;a href=&#34;//localhost:1313/usecases/alvarez_party_2004/&#34; &gt;Alvarez and Nagler (2004)&lt;/a&gt;, measures the dispersion of voters&amp;rsquo; ideological positions relative to the dispersion of parties&amp;rsquo; positions. Alvarez and Nagler apply the measure to data from the American National Election Study (ANES), the British Election Study (BES), and the Canadian Election Study (CES). In principle, it can also be applied to other survey data, such as the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt;.&lt;/p&gt;&#xA;&lt;h2 id=&#34;operationalization&#34;&gt;Operationalization&lt;/h2&gt;&#xA;&lt;p&gt;Alvarez and Nagler calculated an unweighted and a weighted version of the measure, the latter using the parties&amp;rsquo; vote shares as weights. Their measures of party-system compactness are calculated as follows:&#xA;$$CP_k = \frac{\sigma_k}{max|(P_{jk}-P_{lk})|} \forall j,l$$&#xA;where $CP_k$ is the party-system compactness on issue $k$, $P_{jk}$ is the position of party $j$ on issue $k$, $P_{lk}$ is the position of party $l$ and issue $k$, and $\sigma_k$ is the standard deviation of respondents&amp;rsquo; position on issue $k$. The weighted version is calculated as follows:&#xA;$VWCP_k = \frac{\sigma_k}{\sum_{j=1}^N V_j|(P_{jk}-\bar{P_{k}})|}$&#xA;where this formula uses the same concepts and adds $V_j$, which is the vote share of the $j$th party.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Party-System Extremism</title>
      <link>//localhost:1313/measures/party-system_extremism/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/party-system_extremism/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;Party-system extremism, proposed by &lt;a href=&#34;//localhost:1313/usecases/ezrow_parties_2008/&#34; &gt;Ezrow (2008)&lt;/a&gt;, measures the dispersion of parties&amp;rsquo; ideological positions relative to the dispersion of voters&amp;rsquo; positions, borrowing from &lt;a href=&#34;//localhost:1313/usecases/alvarez_party_2004/&#34; &gt;Alvarez and Nagler (2004)&lt;/a&gt;. Ezrow applies the measure to data from the &lt;a href=&#34;//localhost:1313/data/eb/&#34; &gt;Eurobarometer&lt;/a&gt;, while &lt;a href=&#34;//localhost:1313/usecases/dow_partysystem_2011/&#34; &gt;Dow (2011)&lt;/a&gt; uses &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt; data.&lt;/p&gt;&#xA;&lt;h2 id=&#34;operationalization&#34;&gt;Operationalization&lt;/h2&gt;&#xA;&lt;p&gt;The weighted version computes as follows:&lt;/p&gt;&#xA;&lt;p&gt;$$WPE_k=\frac{\sum_{j=1}VS_j|(P_{jk}-\bar{V}_k)|}{\sigma _{V_k}}$$&lt;/p&gt;&#xA;&lt;p&gt;where: $V_k$ is the average left-right self-placement of respondents in country-year $k$, $P_{jk}$ is the ideological position of party $j$ in country-year $k$, $VS_j$ is the vote share for party $j$, and $\sigma_{vk}$ is the standard deviation of respondents&amp;rsquo; left-right self-placement in country-year $k$.&#xA;The unweighted version is computed as follows:&lt;/p&gt;</description>
    </item>
    <item>
      <title>Polarization Index</title>
      <link>//localhost:1313/measures/polarization-index/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/polarization-index/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;&lt;a href=&#34;//localhost:1313/usecases/dalton_quantity_2008/&#34; &gt;Dalton (2008)&lt;/a&gt; proposes the Polarization Index, which measures ideological polarization among parties based on &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt; respondents&amp;rsquo; placement of parties on a left-right scale. It is the most widely used measure of ideological polarization and can also be used to measure ideological polarization at the mass level (see &lt;a href=&#34;//localhost:1313/usecases/reiljan_fear_2020/&#34; &gt;Reiljan (2020)&lt;/a&gt;).&lt;/p&gt;&#xA;&lt;h2 id=&#34;operationalization&#34;&gt;Operationalization&lt;/h2&gt;&#xA;&lt;p&gt;The Polarization Index can be calculated as follows. First, the party positions are calculated as an average of the left-right scores assigned to parties by respondents, and then the polarization index is calculated using the following formula:&lt;/p&gt;</description>
    </item>
    <item>
      <title>Range</title>
      <link>//localhost:1313/measures/range/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/range/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;The range between the two most distant parties on the left-right dimension in a party system is one of the earliest, first used by &lt;a href=&#34;//localhost:1313/usecases/crepaz_impact_1990/&#34; &gt;Crepaz (1990)&lt;/a&gt;, and most widely used measures of ideological polarization. While the measure has mostly been applied to &lt;a href=&#34;//localhost:1313/data/ches/&#34; &gt;CHES&lt;/a&gt; expert ratings and &lt;a href=&#34;//localhost:1313/data/marpor/&#34; &gt;MARPOR&lt;/a&gt; estimates of party positions, it is in principle applicable to any estimates of party positions, regardless of their source.&lt;/p&gt;&#xA;&lt;h2 id=&#34;operationalization&#34;&gt;Operationalization&lt;/h2&gt;&#xA;&lt;p&gt;The range is defined as follows:&#xA;$$range = max(lr_p)-min(lr_p)$$&lt;/p&gt;</description>
    </item>
    <item>
      <title>SD</title>
      <link>//localhost:1313/measures/sd/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/sd/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;Of course, polarization, i.e. the distribution of party or voter positions, can also be measured by calculating the standard deviation. It is used by a number of scholars, primarily to measure ideological polarization among the mass public, based on &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt;, &lt;a href=&#34;//localhost:1313/data/eb/&#34; &gt;Eurobarometer&lt;/a&gt; and &lt;a href=&#34;//localhost:1313/data/ees/&#34; &gt;EES&lt;/a&gt; data.&lt;/p&gt;&#xA;&lt;h2 id=&#34;operationalization&#34;&gt;Operationalization&lt;/h2&gt;&#xA;&lt;p&gt;The standard deviation of party or voter positions is calculated as follows:&#xA;$$s_i = \sqrt{\frac{\sum_{i=1}^n(p_i-\bar{p})^2}{n}}$$&#xA;where the subscript $i$ stands for an individual respondent or party, $p_i$ is the position of a respondent or party $i$, $\bar{p}$ is the average position of all respondents or parties, and $n$ is the number of parties or respondents.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Social Distance Scale</title>
      <link>//localhost:1313/measures/socialdistance/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/socialdistance/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;Other than most of the other measures in this list, the Social Distance Scale does not compute an affective polarization score from other discrete items, but consists of one survey item that asks respondents of the closest relationship they would find acceptable with outparty voters. This way, it is supposed to measure not just like and dislike scores that are unclear in their implications, but the real societal impact of partisan differences.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Spread</title>
      <link>//localhost:1313/measures/spread/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/spread/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;The (weighted) spread of like-dislike scores is a measure originally proposed by Wagner (2020), who considers it superior to the &lt;a href=&#34;//localhost:1313/measures/distance/&#34; &gt;distance&lt;/a&gt; from the most-liked party, and has since been used by a number of scholars to measure affective polarization. It measures the average spread of party like-dislike scores within respondents, weighted by the size of the parties. To aggregate to the party system, one simply calculates the unweighted average of all respondents&amp;rsquo; &lt;a href=&#34;//localhost:1313/measures/spread/&#34; &gt;spread&lt;/a&gt; scores. Wagner computes the (weighted) spread of like-dislike scores based on the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt; dataset, but in principle it can be applied to other datasets that contain the required items, i.e., like-dislike scores.&lt;/p&gt;</description>
    </item>
    <item>
      <title>Variance</title>
      <link>//localhost:1313/measures/variance-weighted/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      <guid>//localhost:1313/measures/variance-weighted/</guid>
      <description>&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;&#xA;&lt;p&gt;The weighted variance of party ideology scores was the first measure used to measure ideological polarization. It was first used by &lt;a href=&#34;//localhost:1313/usecases/taylor_party_1971/&#34; &gt;Taylor and Herman (1971)&lt;/a&gt; and is still very popular. The variance formula includes weights to account for the different sizes of the parties, which some operationalize as seat shares (e.g., &lt;a href=&#34;//localhost:1313/usecases/taylor_party_1971/&#34; &gt;Taylor and Herman 1971&lt;/a&gt;, &lt;a href=&#34;//localhost:1313/usecases/hazan_centre_1997/&#34; &gt;Hazan 1997&lt;/a&gt;), while others use vote shares (e.g., &lt;a href=&#34;//localhost:1313/usecases/bejar_elite_2020/&#34; &gt;Béjar et al. 2020&lt;/a&gt;, &lt;a href=&#34;//localhost:1313/usecases/sigelman_leftright_1978/&#34; &gt;Sigelman and Yough 1978&lt;/a&gt;). The measure has been applied to a variety of data sources, including the &lt;a href=&#34;//localhost:1313/data/cses/&#34; &gt;CSES&lt;/a&gt;, &lt;a href=&#34;//localhost:1313/data/wvs/&#34; &gt;WVS&lt;/a&gt;, and &lt;a href=&#34;//localhost:1313/data/marpor/&#34; &gt;MARPOR&lt;/a&gt;.&lt;/p&gt;</description>
    </item>
  </channel>
</rss>
