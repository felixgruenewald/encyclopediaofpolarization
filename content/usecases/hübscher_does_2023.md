---
authors: Evelyne Hübscher, Thomas Sattler, Markus Wagner
date: 2023-01-01
title: Does Austerity Cause Polarization?
subtitle: 
publication: British Journal of Political Science
doi: https://doi.org/10.1017/S0007123422000734
polarisation:
  - ideological
  - elite
measures:
  - variance-weighted
data:
  - marpor
  - other
aliases:
  - Hübscher et al. (2023)
---
## Abstract
In recent decades, governments in many Western democracies have shown a remarkable consensus in pursuing fiscal austerity measures during periods of strained public finances. In this article, we show that these decisions have consequences for political polarization. Our macro-level analysis of 166 elections since 1980 finds that austerity measures increase both electoral abstention and votes for non-mainstream parties, thereby boosting party system polarization. A detailed analysis of selected austerity episodes also shows that new, small and radical parties benefit most from austerity policies. Finally, survey experiments with a total of 8,800 respondents in Germany, Portugal, Spain and the UK indicate that the effects of austerity on polarization are particularly pronounced when the mainstream right and left parties both stand for fiscal restraint. Austerity is a substantial cause of political polarization and hence political instability in industrialized democracies.