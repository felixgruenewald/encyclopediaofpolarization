---
authors: Peter Mair, Francis G. Castles
date: 1997-01-01
title: Reflections
subtitle: Revisiting expert judgements
publication: European Journal of Political Research
doi: 
polarisation:
  - ideological
  - elite
measures:
  - range
data:
  - other
aliases:
  - Mair and Castles (1997)
---
## Abstract
