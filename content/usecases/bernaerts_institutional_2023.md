---
authors: Kamil Bernaerts, Benjamin Blanckaert, Didier Caluwaerts
date: 2023-01-01
title: Institutional design and polarization
subtitle: Do consensus democracies fare better in fighting polarization than majoritarian democracies?
publication: Democratization
doi: https://doi.org/10.1080/13510347.2022.2117300
polarisation:
  - affective
  - ideological
  - mass
measures: 
data:
  - other
aliases:
  - Bernaerts et al. (2023)
---

## Abstract
It is often claimed that we are living in an age of increasing polarization. Political views, opinions, and worldviews become increasingly irreconcilable (idea-based polarization), while at the same time society appears to be getting fractured in antagonistic, opposing camps (identity-based polarization). However, a closer look at international datasets reveals that these forms of polarization do not affect all democracies to the same extent. Levels of identity-based and idea-based polarization strongly vary across countries. The question then becomes what can explain these diverging levels of polarization. In this paper, we hypothesize that the institutional design of a country impacts polarization, and that consensus democracies would display lower levels of polarization. Based on a quantitative analysis of the Comparative Political Dataset and Varieties of Democracy data in 36 countries over time (2000–2019), our results show that institutions did matter to a great extent, and in the hypothesized direction. Countries with consensus institutions, and more in particular PR electoral systems, multiparty coalitions, and federalism did exhibit lower levels of both issue-based and identity-based polarization, thereby confirming our expectations. Moreover, we found that consensus democracies tend to be better at coping with identity-based polarization, while the effect on idea-based polarization is smaller.