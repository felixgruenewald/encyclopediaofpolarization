---
authors: James N Druckman, Matthew S. Levendusky
date: 2019-01-01
title: What Do We Measure When We Measure Affective Polarization?
subtitle: 
publication: Public Opinion Quarterly
doi: https://doi.org/10.1093/poq/nfz003
polarisation:
  - affective
  - elite
  - mass
  - vertical
  - horizontal
measures:
  - socialdistance
data:
  - other
aliases:
  - Druckman and Levendusky (2019)
---

## Abstract
Affective polarization—the tendency of Democrats and Republicans to dislike and distrust one another—has become an important phenomenon in American politics. Yet, despite scholarly attention to this topic, two measurement lacunae remain. First, how do the different measures of this concept relate to one another—are they interchangeable? Second, these items all ask respondents about the parties. When individuals answer them, do they think of voters, elites, or both? We demonstrate differences across items, and scholars should carefully think about which items best match their particular research question. Second, we show that when answering questions about the other party, individuals think about elites more than voters. More generally, individuals dislike voters from the other party, but they harbor even more animus toward the other party’s elites. The research note concludes by discussing the consequences for both measuring this concept and understanding its ramifications.