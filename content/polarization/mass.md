---
title: Mass Polarization
weight: 4
---

A large number of measures can be classified as measures of mass polarization, i.e., polarization that is measured between members of the broader public. This is to a great degree due to the fact that the most commonly available datasets are general population surveys. However, one should still distinct whether the measures are directed at other members of the public ([horizontal polarization]({{< ref "horizontal" >}})) or at political elites ([vertical polarization]({{< ref "vertical" >}})). One could, for example, ask for opinions of the voters of a certain party, or of the party and its officials itself. 

{{< citation >}}

## Measures

| Measure                                                | Polarization                           |
| ------------------------------------------------------ | -------------------------------------- |
| [API]({{< ref "measures/api.md" >}})                               | affective, mass, vertical              |
| [Distance]({{< ref "measures/distance.md" >}})                     | affective, mass, vertical              |
| [Party Dyads]({{< ref "measures/dyads.md" >}})                     | ideological, affective, mass, vertical |
| [Polarization Index]({{< ref "measures/polarization-index.md" >}}) | ideological, elite, mass               |
| [SD]({{< ref "measures/sd.md" >}})                                 | ideological, elite, mass               |
| [Spread]({{< ref "measures/spread.md" >}})                         | affective, mass, vertical              |
| [Social Distance Scale]({{< ref "measures/socialdistance.md" >}})  | affective, mass, horizontal            |

## Use cases

| Title                                                                                                                                          | Authors                        |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| [Is Polarization a Myth?]({{< ref "usecases/abramowitz_polarization_2008.md" >}})                                                                          | Abramowitz and Saunders (2008) |
| [Can’t We All Just Get Along? How Women MPs Can Ameliorate Affective Polarization in Western Publics]({{< ref "usecases/adams_can_2023.md" >}})            | Adams et al. (2023)            |
| [The Downsian roots of affective polarization]({{< ref "usecases/algara_downsian_2023.md" >}})                                                             | Algara and Zur (2023)          |
| [Elite polarization, party extremity, and affective polarization]({{< ref "usecases/banda_elite_2018.md" >}})                                              | Banda and Cluverius (2018)     |
| [Has Trust in the European Parliament Polarized?]({{< ref "usecases/bauer_has_2023.md" >}})                                                                | Bauer and Morisi (2023)        |
| [Elite communication and affective polarization among voters]({{< ref "usecases/bäck_elite_2023.md" >}})                                                   | Bäck et al. (2023)             |
| [Institutional design and polarization]({{< ref "usecases/bernaerts_institutional_2023.md" >}})                                                            | Bernaerts et al. (2023)        |
| [Consequences of affective polarization]({{< ref "usecases/berntzen_consequences_2023.md" >}})                                                             | Berntzen et al. (2023)         |
| [A regional perspective to the study of affective polarisation]({{< ref "usecases/bettarelli_regional_2023.md" >}})                                        | Bettarelli et al. (2023)       |
| [Do Voters Polarize When Radical Parties Enter Parliament?]({{< ref "usecases/bischof_voters_2019.md" >}})                                                 | Bischof and Wagner (2019)      |
| [Cleavage politics, polarisation and participation in Western Europe]({{< ref "usecases/borbath_cleavage_2023.md" >}})                                     | Borbáth et al. (2023)          |
| [Understanding Polarization]({{< ref "usecases/bramson_understanding_2017.md" >}})                                                                         | Bramson et al. (2017)          |
| [The Politics of Interpersonal Trust and Reciprocity]({{< ref "usecases/carlin_politics_2013.md" >}})                                                      | Carlin and Love (2013)         |
| [What can De-Polarize the Polarizers?]({{< ref "usecases/ciobanu_what_2022.md" >}})                                                                        | Ciobanu and Sandu (2022)       |
| [Ideological identity, issue-based ideology and bipolar affective polarization in multiparty systems]({{< ref "usecases/comellas_ideological_2023.md" >}}) | Comellas and Torcal (2023)     |
| [When polarised feelings towards parties spread to voters]({{< ref "usecases/comellasbonsfills_when_2022.md" >}})                                          | Comellas Bonsfills (2022)      |
| [What Do We Measure When We Measure Affective Polarization?]({{< ref "usecases/druckman_what_2019.md" >}})                                                 | Druckman and Levendusky (2019) |
| [Affective Polarisation in Comparative and Longitudinal Perspective]({{< ref "usecases/garzia_affective_2023.md" >}})                                      | Garzia et al. (2023)           |
| [American Affective Polarization in Comparative Perspective]({{< ref "usecases/gidron_american_2020.md" >}})                                               | Gidron et al. (2020)           |
| [Validating the feeling thermometer as a measure of partisan affect in multi-party systems]({{< ref "usecases/gidron_validating_2022.md" >}})              | Gidron et al. (2022)           |
| [Does affective polarisation increase turnout?]({{< ref "usecases/harteveld_does_2023.md" >}})                                                             | Harteveld and Wagner (2023)    |
| [Fragmented foes]({{< ref "usecases/harteveld_fragmented_2021.md" >}})                                                                                     | Harteveld (2021)               |
| [Affective polarization and the salience of elections]({{< ref "usecases/hernández_affective_2021.md" >}})                                                 | Hernández et al. (2021)        |
| [Divided by the Vote]({{< ref "usecases/hobolt_divided_2020.md" >}})                                                                                       | Hobolt et al. (2020)           |
| [Affect, Not Ideology]({{< ref "usecases/iyengar_affect_2012.md" >}})                                                                                      | Iyengar et al. (2012)          |
| [Fear and Loathing across Party Lines]({{< ref "usecases/iyengar_fear_2015.md" >}})                                                                        | Iyengar and Westwood (2015)    |
| [Sweet victory, bitter defeat]({{< ref "usecases/janssen_sweet_2023.md" >}})                                                                               | Janssen (2023)                 |
| [Affective Polarization and Misinformation Belief]({{< ref "usecases/jenke_affective_2023.md" >}})                                                         | Jenke (2023)                   |
| [The Bipolar Voter]({{< ref "usecases/moral_bipolar_2017.md" >}})                                                                                          | Moral (2017)                   |
| [On the relationship between party polarisation and citizen polarisation]({{< ref "usecases/moral_relationship_2023.md" >}})                               | Moral and Best (2023)          |
| [The relationship between affective polarisation and democratic backsliding]({{< ref "usecases/orhan_relationship_2022.md" >}})                            | Orhan (2022)                   |
| [Measuring partisan polarization with partisan differences in satisfaction with the government]({{< ref "usecases/patkós_measuring_2023.md" >}})           | Patkós (2023)                  |
| [Polarization and correct voting in U.S. presidential elections]({{< ref "usecases/pierce_polarization_2019.md" >}})                                       | Pierce and Lau (2019)          |
| [Fear and loathing across party lines (also) in Europe]({{< ref "usecases/reiljan_fear_2020.md" >}})                                                       | Reiljan (2020)                 |
| [Patterns of Affective Polarisation toward Parties and Leaders across the Democratic World]({{< ref "usecases/reiljan_patterns_2023.md" >}})               | Reiljan et al. (2023)          |
| [Overlapping polarization]({{< ref "usecases/riera_overlapping_2023.md" >}})                                                                               | Riera and Madariaga (2023)     |
| [Polarisation, identity and affect]({{< ref "usecases/rollicke_polarisation_2023.md" >}})                                                                  | Röllicke (2023)                |
| [Exploring differences in affective polarization between the Nordic countries]({{< ref "usecases/ryan_exploring_2023.md" >}})                              | Ryan (2023)                    |
| [Intergroup contact reduces affective polarization but not among strong party identifiers]({{< ref "usecases/thomsen_intergroup_2023.md" >}})              | Thomsen and Thomsen (2023)     |
| [What Do We Measure When We Measure Affective Polarization across Countries?]({{< ref "usecases/tichelbaecker_what_2023.md" >}})                           | Tichelbaecker et al. (2023)    |
| [Social trust and affective polarization in Spain (2014–19)]({{< ref "usecases/torcal_social_2023.md" >}})                                                 | Torcal and Thomson (2023)      |
| [Group-based public opinion polarisation in multi-party systems]({{< ref "usecases/traber_groupbased_2023.md" >}})                                         | Traber et al. (2023)           |
| [Affective polarization and coalition signals]({{< ref "usecases/wagner_affective_2023.md" >}})                                                            | Wagner and Praprotnik (2023)   |
| [Affective polarization in Europe]({{< ref "usecases/wagner_affective_2024.md" >}})                                                                        | Wagner (2024)                  |
| [Divided by the jab]({{< ref "usecases/wagner_divided_2024.md" >}})                                                                                        | Wagner and Eberl (2024)        |
| [The tie that divides]({{< ref "usecases/westwood_tie_2018.md" >}})                                                                                        | Westwood et al. (2018)         |

