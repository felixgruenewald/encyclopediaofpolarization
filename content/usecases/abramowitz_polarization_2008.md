---
authors: Alan I. Abramowitz, Kyle L. Saunders
date: 2008-01-01
title: Is Polarization a Myth?
subtitle: 
publication: The Journal of Politics
doi: https://doi.org/10.1017/s0022381608080493
polarisation:
  - ideological
  - mass
measures: 
data:
  - other
aliases:
  - Abramowitz and Saunders (2008)
---
## Abstract
This article uses data from the American National Election Studies and national exit polls to test Fiorina's assertion that ideological polarization in the American public is a myth. Fiorina argues that twenty-first-century Americans, like the midtwentieth-century Americans described by Converse, “are not very well-informed about politics, do not hold many of their views very strongly, and are not ideological” (2006, 19). However, our evidence indicates that since the 1970s, ideological polarization has increased dramatically among the mass public in the United States as well as among political elites. There are now large differences in outlook between Democrats and Republicans, between red state voters and blue state voters, and between religious voters and secular voters. These divisions are not confined to a small minority of activists—they involve a large segment of the public and the deepest divisions are found among the most interested, informed, and active citizens. Moreover, contrary to Fiorina's suggestion that polarization turns off voters and depresses turnout, our evidence indicates that polarization energizes the electorate and stimulates political participation.
