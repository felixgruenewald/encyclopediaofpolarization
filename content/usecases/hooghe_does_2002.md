---
authors: Liesbet Hooghe, Gary Marks, Carole J. Wilson
date: 2002-01-01
title: Does Left/Right Structure Party Positions on European Integration?
subtitle: 
publication: Comparative Political Studies
doi: https://doi.org/10.1177/001041402236310
polarisation:
  - ideological
  - elite
measures: 
data: 
  - other
aliases:
  - Hooghe et al. (2002)
---

## Abstract
How is contestation on European integration structured among national political parties? Are issues arising from European integration assimilated into existing dimensions of domestic contestation? We show that there is a strong relationship between the conventional left/right dimension and party positioning on European integration. However, the most powerful source of variation in party support is the new politics dimension, ranging from Green/alternative/libertarian to Traditional/authoritarian/nationalist.