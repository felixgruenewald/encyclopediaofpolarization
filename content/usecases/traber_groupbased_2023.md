---
authors: Denise Traber, Lukas F. Stoetzer, Tanja Burri
date: 2023-01-01
title: Group-based public opinion polarisation in multi-party systems
subtitle: 
publication: West European Politics
doi: https://doi.org/10.1080/01402382.2022.2110376
polarisation:
  - ideological
  - mass
measures: 
data:
  - other
aliases:
  - Traber et al. (2023)
---
## Abstract
Public opinion polarisation can impair society’s ability to reach a democratic consensus in different political issue areas. This appears particularly true when the polarisation of opinions coincides with clearly identifiable social groups. The literature on public opinion polarisation has mostly focussed on the US two-party context. We lack concepts and measures that can be adapted to European countries with multi-party systems and multi-layered group identities. This article proposes a conceptualisation of polarisation between groups in society. It presents a measure that captures the overlap of ideology distributions between groups. The two-step empirical framework includes hierarchical IRT models and a measure for dissimilarity of distributions. The second part presents an empirical application of the measure based on survey data from Switzerland (1994–2016), which reveals insightful dynamics of public opinion polarisation between party supporters and education groups.