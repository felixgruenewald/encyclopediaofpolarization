---
authors: Federico Vegetti
date: 2014-01-01
title: From political conflict to partisan evaluations
subtitle: How citizens assess party ideology and competence in polarized elections
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2014.01.007
polarisation:
  - ideological
  - elite
measures:
  - sd
data:
  - ees
aliases:
  - Vegetti (2014)
---

## Abstract
Recent comparative electoral research shows that both ideological and competence voting are influenced by the degree of party system polarization. However, while the former association is uncontroversial, investigations on the latter have led to contradicting results. This study takes one step back, arguing that polarization rather affects how voters perceive party ideological positioning and competence. Building on literature linking elite polarization to mass partisanship, the study argues that party identification is a strong moderator of party evaluations in polarized elections. Hypotheses are tested with multilevel logit models on a pooled data set of European Election Studies from 1994 to 2009. Results show that partisans are more likely to view their preferred party as the most competent and ideologically close when the environment is polarized, while there is no such effect for non-partisans.
