---
title: Encyclopedia of Polarization
---
<!-- This is the title page -->
![](/images/polarlogo.png) 

# Welcome!
This website presents the **Encyclopedia of Polarization** and the accompanying R package [polaR]({{< ref "polarpackage" >}}).

## 📘 Encyclopedia of Polarization

The *Encyclopedia of Polarization* is an online repository that catalogs available measures of political polarization and their use in applied research, as well as data sources for cross-national comparative research. It currently describes eleven measures of political polarization used in comparative research.

The *Encyclopedia* helps researchers navigate the field by categorizing extant approaches by their different understandings of [Polarization]({{< ref "Polarization" >}}), the [Measures]({{< ref "Measures" >}}) used to capture them, [Variables]({{< ref "Variables" >}}) surveyed and the [Data]({{< ref "Data" >}}) they occur in, and by providing a comprehensive list of [use cases]({{< ref "usecases" >}}). 

This repository is in continuing development and aims to provide a good introduction to the topic, rather than a comprehensive review of this quickly developing field. We are grateful for any comments or suggestions on how to develop this collection further.

## ❄️ polaR

[polaR]({{< ref "polarpackage" >}}) is an R package that we are currently developing, which implements the measures documented in the *Encyclopedia* and provides functions for importing the most commonly used cross-national comparative datasets. The package currently supports six of the datasets and ten of the measures of polarization documented in the Encyclopedia. The development version of the package is available on [GitLab](https://gitlab.com/felixgruenewald/polaR) and comments and feedback are very welcome. 

## 👥 The team

The *Encyclopedia of Polarization* is developed by the Emmy Noether research group "Polarization through and in referendums: mapping polarization within and beyond the party system". This group is based at [Chemnitz University of Technology](https://tu-chemnitz.de/polmeth) and led by [Arndt Leininger](https://aleininger.eu/). [Felix Grünewald](https://felixgruenewald.net/) leads the development of the *Encyclopedia* and [polaR]({{< ref "polarpackage" >}}), while [Nelly Buntfuß](https://www.researchgate.net/profile/Nelly-Buntfuss) oversees the polarization literature review. We are supported by our excellent research assistants [Marcel Schneider](https://www.tu-chemnitz.de/phil/politik/pf/professur/Schneider.php.en) and [Lina Zündorf](https://www.tu-chemnitz.de/phil/politik/pf/professur/Zundorf.php.en).

We also work on a literature and methods review on polarization in multiparty systems, which you can find on [SocArXiv](https://osf.io/preprints/socarxiv/mz6rs/). Please cite this paper if you are using the contents of this website or our R package for your own work: 

{{< citation >}}


<iframe src="https://mfr.osf.io/render?url=https://osf.io/download/64d606602fe4965c8e61b130/?direct%26mode=render"
frameborder="0"
scrolling="no"
style="overflow:hidden;height:800;width:100%"
height="800"
width="100%"></iframe>

Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) – **Projektnummer [491988424](https://gepris.dfg.de/gepris/projekt/491988424)**