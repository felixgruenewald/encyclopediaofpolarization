---
authors: J. Areal, E. Harteveld
date: 2024-01-01
title: Vertical vs horizontal affective polarization Disentangling feelings towards elites and voters
subtitle:
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2024.102814
polarisation:
  - vertical
  - horizontal
  - mass
  - affective
measures:
data:
  - other
aliases:
  - Areal and Harteveld (2024)
tags:
---

## Abstract

The way people feel towards other voters has garnered enormous attention with the rise of affective polarization, or hostility across political lines. As this literature grows increasingly comparative, scholars often rely on the widely available feeling thermometer towards political parties. This carries the strong assumption that (dis)affect towards parties (“vertical”) extends to voters (“horizontal”). We test this assumption using 14 independent samples covering 10 countries. Firstly, we ask whether people differentiate between parties/politicians and their voters. We find that individuals consistently differentiate between elites and voters, though this is conditional on whether evaluations are towards in- or out-groups. Secondly, we examine which factors are associated with a greater gap in evaluations. We find that differentiation may be more related to the type of party-voter group being evaluated rather than individual-level features. Put together, these findings suggest researchers should be cautious when equating vertical and horizontal affective polarization.