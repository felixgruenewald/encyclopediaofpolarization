---
authors: Levi Boxell, Matthew Gentzkow, Jesse M. Shapiro
date: 2022-01-01
title: Cross-Country Trends in Affective Polarization
subtitle: 
publication: The Review of Economics and Statistics
doi: https://doi.org/10.1162/rest_a_01160
polarisation:
  - mass
  - vertical
  - affective
measures:
  - distance
data:
  - cses
  - other
aliases:
  - Boxell et al. (2022)
---

## Abstract

We measure trends in affective polarization in twelve OECD countries over the past four decades. According to our baseline estimates, the US experienced the largest increase in polarization over this period. Five countries experienced a smaller increase in polarization. Six countries experienced a decrease in polarization. We relate trends in polarization to trends in potential explanatory factors.