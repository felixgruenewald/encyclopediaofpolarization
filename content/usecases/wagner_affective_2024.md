---
authors: Markus Wagner
date: 2024-01-01
title: Affective polarization in Europe
subtitle: 
publication: European Political Science Review
doi: https://doi.org/10.1017/S1755773923000383
polarisation:
  - affective
  - mass
measures: 
data: 
aliases:
  - Wagner (2024)
---

## Abstract
Affective polarization, a concept that originated in the USA, has increasingly been studied in Europe’s multi-party systems. This form of polarization refers to the extent to which party supporters dislike one another – or, more technically, to the difference between the positive feelings towards the supporters of one’s own political party and the negative feelings towards the supporters of other parties. Measuring this gap in Europe’s multi-party systems requires researchers to make various important decisions relating to conceptualization and measurement. Often, our focus could instead lie on assessing partisan hostility or negative party affect, which is easier to measure. While recent research on affective polarization in Europe has already taught USA lot, both about affective polarization and about political conflict in Europe, I nevertheless suggest that research in this field faces four challenges, namely developing better measures, more sophisticated theories, clearer accounts of affective polarization’s importance and successful ways of reducing negative party affect, if this is indeed desirable.