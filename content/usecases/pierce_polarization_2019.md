---
authors: Douglas R. Pierce, Richard R. Lau
date: 2019-01-01
title: Polarization and correct voting in U.S. presidential elections
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2019.102048
polarisation:
  - elite
  - mass
  - ideological
measures: 
data:
  - other
aliases:
  - Pierce and Lau (2019)
---

## Abstract
Although some political pundits have expressed concern that political polarization has a deleterious effect on voter behavior, others have argued that polarization may actually benefit voters by presenting citizens with clear choices between the two major parties. We take up this question by examining the effects of polarization on the quality of voter decision making in U.S. presidential elections. We find that ideological polarization among elites, along with ideological sorting and affective polarization among voters, all contribute to the probability of citizens’ voting correctly. Furthermore, affective polarization among the citizenry if anything strengthens, not weakens, the influence of political knowledge on voter decision-making. We conclude that to the extent that normative democratic theory supposes that people vote for candidates who share their interests, polarization has had a positive effect on voter decision-making quality, and thus democratic representation, in the United States.
