---
authors: Sean J. Westwood, Shanto Iyengar, Stefaan Walgrave, Rafael Leonisio, Luis Miller, Oliver Strijbis
date: 2018-01-01
title: The tie that divides
subtitle: Cross-national evidence of the primacy of partyism
publication: European Journal of Political Research
doi: https://doi.org/10.1111/1475-6765.12228
polarisation:
  - affective
  - mass
  - horizontal
measures: 
data:
  - other
aliases:
  - Westwood et al. (2018)
---

## Abstract
Using evidence from Great Britain, the United States, Belgium and Spain, it is demonstrated in this article that in integrated and divided nations alike, citizens are more strongly attached to political parties than to the social groups that the parties represent. In all four nations, partisans discriminate against their opponents to a degree that exceeds discrimination against members of religious, linguistic, ethnic or regional out-groups. This pattern holds even when social cleavages are intense and the basis for prolonged political conflict. Partisan animus is conditioned by ideological proximity; partisans are more distrusting of parties furthest from them in the ideological space. The effects of partisanship on trust are eroded when partisan and social ties collide. In closing, the article considers the reasons that give rise to the strength of ‘partyism’ in modern democracies.