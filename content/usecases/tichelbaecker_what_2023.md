---
authors: Thomas Tichelbaecker, Noam Gidron, Will Horne, James Adams
date: 2023-01-01
title: What Do We Measure When We Measure Affective Polarization across Countries?
subtitle: 
publication: Public Opinion Quarterly
doi: https://doi.org/10.1093/poq/nfad033
polarisation:
  - affective
  - elite
  - mass
  - vertical
  - horizontal
measures:
  - socialdistance
data:
  - other
aliases:
  - Tichelbaecker et al. (2023)
---

## Abstract
Measures of affective polarization—that is, dislike and hostility across party lines—have been developed and validated in the context of America’s two-party system. Yet increasingly, affective polarization is examined comparatively. We address this issue by introducing a novel dataset that measures aspects of partisan affect in 10 countries with diverse party systems. We detect associations between partisan affect toward out-parties versus affect toward out-parties’ supporters, but their strength varies across countries. We discuss measurement reasons for this variation and consider the implications of our findings for the emerging comparative affective polarization literature.