---
authors: I.D. Mehlhaff
date: 2023-01-01
title: A Group-Based Approach to Measuring Polarization
subtitle:
publication: American Political Science Review
doi: https://doi.org/10.1017/S0003055423001041
polarisation:
  - affective
  - horizontal
  - elite
  - vertical
measures:
data:
  - other
aliases:
  - Mehlhaff (2023)
tags:
---

## Abstract

Despite polarization’s growing importance in social science, its quantitative measurement has lagged behind its conceptual development. Political and social polarization are group-based phenomena characterized by intergroup heterogeneity and intragroup homogeneity, but existing measures capture only one of these features or make it difficult to compare across cases or over time. To bring the concept and measurement of polarization into closer alignment, I introduce the clusterpolarization coefficient (CPC), a measure of multimodality that allows scholars to incorporate multiple variables and compare across contexts with varying numbers of parties or social groups. Three applications to elite and mass polarization demonstrate that the CPC returns substantively sensible results, and an open-source software package implements the measure.