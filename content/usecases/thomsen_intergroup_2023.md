---
authors: Jens Peter Frølund Thomsen, Anna Håland Thomsen
date: 2023-01-01
title: Intergroup contact reduces affective polarization but not among strong party identifiers
subtitle: 
publication: Scandinavian Political Studies
doi: https://doi.org/10.1111/1467-9477.12242
polarisation:
  - affective
  - horizontal
  - mass
measures:
  - distance
data:
  - other
aliases:
  - Thomsen and Thomsen (2023)
---

## Abstract
Previous studies have assumed that the relationship between intergroup contact and affective polarization is uniform across political predispositions. We argue instead that party identification serves as a boundary condition for the intergroup contact‒affective polarization relationship. Our findings suggest that: (1) intergroup contact between “in-party” and “out-party” supporters reduces affective polarization among nonidentifiers, weak, and moderate party identifiers, and (2) intergroup contact remains unrelated to affective polarization among strong party identifiers. These findings apply to high- and low-quality contact measures alike, meaning that party identification strength is an essential boundary condition capable of obstructing the impact of contact on affective polarization. Analyses were performed on a new sample among Danish citizens. The survey was fielded in 2021 (n = 3638). The findings contribute to both intergroup contact research and to the study of the sources of affective polarization. In terms of broader implications, our study suggests that aside from true partisans, intergroup contact stimulates democratic behavior.