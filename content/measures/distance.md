---
title: Distance
name: (Weighted) mean distance from the most-liked party
polarisation:
  - affective
  - mass
  - vertical
data:
  - cses
variables:
  - like-dislike
usecases:
  - wagner_affective_2021
  - bäck_elite_2023
  - comellas_ideological_2023
  - druckman_what_2019
  - gidron_validating_2022
  - hernández_affective_2021
  - iyengar_affect_2012
  - riera_overlapping_2023
  - ryan_exploring_2023
  - steiner_economic_2012
  - thomsen_intergroup_2023
  - tichelbaecker_what_2023
  - torcal_social_2023
  - wagner_affective_2023
math: true
---
## Description
The (weighted) mean distance from the most liked party is a measure suggested by [Wagner (2021)]({{< ref "wagner_affective_2021" >}}), although he considers it inferior to the (weighted) spread. It measures the average discrepancy between like for the most liked party and like/dislike for all other parties. Wagner computes the (weighted) distance based on the [CSES]({{< ref "cses" >}}) dataset, but in principle it can be applied to other datasets that contain the required items, i.e., like-dislike scores.
​
## Operationalization
Wagner proceeds in two steps. First, they calculate the (weighted) mean distance for each respondent. Second, they aggregate to the party-system level by taking the mean of the respondents' distance scores. The unweighted and weighted distance measures are calculated as follows:

Unweighted: $Distance_i = \sqrt{\frac{\sum_{p=1}^p (like_{ip} - like_{max,i})^2}{n_p}}$

Weighted: $Distance_i = \sqrt{\sum_{p=1}^p v_p (like_{ip} - like_{max,i})^2}$

The subscript $i$ denotes an individual respondent, $p$ denotes a party, $max$ is the most liked party, and $v_p$ is the percentage of votes received by a party.
​
## polaR
{{< polarref >}}
```r
# Import Data
cses_imd <- polaR_import(source = "cses_imd",
						 path = "path/to/dataset.dta")

# Use 'weighted' to toggle between the weighted and unweighted measure
# The data output can be provided with individual scores or aggregated to a country/year level with 'aggregate'
cses_imd <- distance(cses_imd,
					 weighted = c(TRUE, FALSE),
					 aggregate = c(TRUE, FALSE))
```
​
## Visualization
{{< shinyapp cses "distance,distance_wgt" >}}

## Use cases
_Publications that use this measure:_

| Title                                                                                                                                          | Authors                        |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| [Elite communication and affective polarization among voters]({{< ref "usecases/bäck_elite_2023.md" >}})                                                   | Bäck et al. (2023)             |
| [Ideological identity, issue-based ideology and bipolar affective polarization in multiparty systems]({{< ref "usecases/comellas_ideological_2023.md" >}}) | Comellas and Torcal (2023)     |
| [What Do We Measure When We Measure Affective Polarization?]({{< ref "usecases/druckman_what_2019.md" >}})                                                 | Druckman and Levendusky (2019) |
| [Validating the feeling thermometer as a measure of partisan affect in multi-party systems]({{< ref "usecases/gidron_validating_2022.md" >}})              | Gidron et al. (2022)           |
| [Affective polarization and the salience of elections]({{< ref "usecases/hernández_affective_2021.md" >}})                                                 | Hernández et al. (2021)        |
| [Affect, Not Ideology]({{< ref "usecases/iyengar_affect_2012.md" >}})                                                                                      | Iyengar et al. (2012)          |
| [Overlapping polarization]({{< ref "usecases/riera_overlapping_2023.md" >}})                                                                               | Riera and Madariaga (2023)     |
| [Exploring differences in affective polarization between the Nordic countries]({{< ref "usecases/ryan_exploring_2023.md" >}})                              | Ryan (2023)                    |
| [Economic Integration, Party Polarisation and Electoral Turnout]({{< ref "usecases/steiner_economic_2012.md" >}})                                          | Steiner and Martin (2012)      |
| [Intergroup contact reduces affective polarization but not among strong party identifiers]({{< ref "usecases/thomsen_intergroup_2023.md" >}})              | Thomsen and Thomsen (2023)     |
| [What Do We Measure When We Measure Affective Polarization across Countries?]({{< ref "usecases/tichelbaecker_what_2023.md" >}})                           | Tichelbaecker et al. (2023)    |
| [Social trust and affective polarization in Spain (2014–19)]({{< ref "usecases/torcal_social_2023.md" >}})                                                 | Torcal and Thomson (2023)      |
| [Affective polarisation in multiparty systems]({{< ref "usecases/wagner_affective_2021.md" >}})                                                            | Wagner (2021)                  |
| [Affective polarization and coalition signals]({{< ref "usecases/wagner_affective_2023.md" >}})                                                            | Wagner and Praprotnik (2023)   |
