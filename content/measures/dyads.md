---
title: Party Dyads
name: Party Dyads and In-Out-Party Differences
polarisation:
  - ideological
  - affective
  - mass
  - vertical
data:
  - cses
variables:
  - party-position
  - like-dislike
usecases:
  - gidron_american_2020
  - bantel_camps_2023
  - adams_can_2023
  - comellasbonsfills_when_2022
math: false
---
In political systems with just two parties, polarization describes the distance between these two poles. Therefore, much of the classical American polarization literature, ideological or affective, relied on the difference between a respondent's inparty and outparty, either based on like-dislike scores or the perception of the parties' positions.

Party dyads can be seen as a related form of measurement in multiparty systems, where the difference between two selected parties is taken as an indication of general polarization. This can be done with just one or with multiple dyads. It describes a very tangible form of polarization that can be used to show how relationships to individual parties are affected ([Bantel (2023)]({{< ref "bantel_camps_2023" >}})) or to compare polarization levels between two- and multiparty systems ([Gidron et al. (2020)]({{< ref "gidron_american_2020" >}})).

## Use cases
_Publications that use this measure:_

| Title                                                                                                                               | Authors                   |
| ----------------------------------------------------------------------------------------------------------------------------------- | ------------------------- |
| [Can’t We All Just Get Along? How Women MPs Can Ameliorate Affective Polarization in Western Publics]({{< ref "usecases/adams_can_2023.md" >}}) | Adams et al. (2023)       |
| [Camps, not just parties]({{< ref "usecases/bantel_camps_2023.md" >}})                                                                          | Bantel (2023)             |
| [When polarised feelings towards parties spread to voters]({{< ref "usecases/comellasbonsfills_when_2022.md" >}})                               | Comellas Bonsfills (2022) |
| [American Affective Polarization in Comparative Perspective]({{< ref "usecases/gidron_american_2020.md" >}})                                    | Gidron et al. (2020)      |
