---
authors: Shanto Iyengar, Sean J. Westwood
date: 2015-01-01
title: Fear and Loathing across Party Lines
subtitle: New Evidence on Group Polarization
publication: American Journal of Political Science
doi: https://doi.org/10.1111/ajps.12152
polarisation:
  - affective
  - mass
  - horizontal
measures: 
data:
  - other
aliases:
  - Iyengar and Westwood (2015)
---

## Abstract
When defined in terms of social identity and affect toward copartisans and opposing partisans, the polarization of the American electorate has dramatically increased. We document the scope and consequences of affective polarization of partisans using implicit, explicit, and behavioral indicators. Our evidence demonstrates that hostile feelings for the opposing party are ingrained or automatic in voters' minds, and that affective polarization based on party is just as strong as polarization based on race. We further show that party cues exert powerful effects on nonpolitical judgments and behaviors. Partisans discriminate against opposing partisans, doing so to a degree that exceeds discrimination based on race. We note that the willingness of partisans to display open animus for opposing partisans can be attributed to the absence of norms governing the expression of negative sentiment and that increased partisan affect provides an incentive for elites to engage in confrontation rather than cooperation.