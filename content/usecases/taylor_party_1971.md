---
authors: Michael Taylor, V. M. Herman
date: 1971-01-01
title: Party Systems and Government Stability
publication: The American Political Science Review
doi: https://doi.org/10.2307/1955041
polarisation:
  - ideological
  - elite
measures:
  - variance-weighted
data:
  - other
aliases:
  - Taylor and Herman (1971)
---
## Abstract
