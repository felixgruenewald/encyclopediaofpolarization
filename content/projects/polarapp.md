---
title: "polaR App"
subtitle: Visualisation Tool
images: 
---

All data mentioned in the encyclopedia and collected using our [R package]({{< ref "polarpackage" >}}) can be viewed using the shinyapp below. You can choose the measures, countries and datasets, and in some cases also the issue dimension that the polarization measure is to be computed on. _After visualization_, you also have the option to download a .csv file of the presented data 
(**Note**: due to data availability, not all polarization measures are available for all countries and years. Gaps in the data are therefore to be expected and not an error in the app).

{{< shinyapp "cses" "api,polarization_index" >}}

The code for this app can be found on [GitLab](https://gitlab.com/felixgruenewald/polarapp).