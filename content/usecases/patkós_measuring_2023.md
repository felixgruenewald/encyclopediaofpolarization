---
authors: Veronika Patkós
date: 2023-01-01
title: Measuring partisan polarization with partisan differences in satisfaction with the government
subtitle: The introduction of a new comparative approach
publication: Quality & Quantity
doi: https://doi.org/10.1007/s11135-022-01350-8
polarisation:
  - ideological
  - mass
measures: 
data:
  - ess
aliases:
  - Patkós (2023)
---

## Abstract
In recent years, the attention of political scientists investigating political polarization has turned from the ideological aspects of polarization to its partisan and affective aspects. This recent turn implied that this area has experienced an urgent need to create appropriate polarization indices that are backed with high-quality data across time and countries to carry out comparative research. This paper argues that existing polarization indices mostly fail to adequately include the most important aspect of polarization, that is, bimodality. To fill this gap, it proposes a partisan polarization index using European Social Survey data on government satisfaction of partisan camps, which is available for 32 European countries between 2002 and 2020 for all in all 214 country-years. That is, the paper offers an insight into trends in partisan polarization for these 214 cases. The analysis of cases shows that in the last two decades polarization hit mostly Southern European countries and some EastCentral European ones, like Cyprus, Spain, Greece, Turkey, Poland and especially, Hungary. Within the realm of possibilities, the paper compares the newly constructed index to other polarization indices.