---
authors: Nils D. Steiner, Christian W. Martin
date: 2012-01-01
title: Economic Integration, Party Polarisation and Electoral Turnout
subtitle: 
publication: West European Politics
doi: https://doi.org/10.1080/01402382.2011.648005
polarisation:
  - ideological
  - elite
measures:
  - distance
data:
  - marpor
aliases:
  - Steiner and Martin (2012)
---

## Abstract
Recent research provides evidence that economic integration has a negative effect on electoral turnout. Taking up these recent findings, this article explores the causal chain in more detail. Specifically, it argues that one way by which economic integration affects the calculus of voting is through the positioning of political parties. The expectation is that the polarisation between parties on an economic left–right scale is lower the more integrated an economy is. Consequently, electoral turnout should be lower with less polarisation in the party system. The article employs aggregate-level data from legislative elections in 24 developed democracies. Using data from the Comparative Manifestos Project, evidence is found not only that economic integration has a negative effect on party polarisation as measured on an economic left–right dimension, but also that this in turn exerts a negative effect on electoral turnout.
