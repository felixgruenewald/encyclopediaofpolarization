---
authors: Erika van Elsas, Toine Fiselier
date: 2023-01-01
title: Conflict or choice? The differential effects of elite incivility and ideological polarization on political support
subtitle: 
publication: Acta Politica
doi: https://doi.org/10.1057/s41269-023-00288-5
polarisation:
  - ideological
  - elite
  - affective
measures:
  - polarization-index
data:
  - other
aliases:
  - van Elsas and Fiselier (2023)
---

## Abstract
How does elite polarization impact citizens’ political support? While elite polarization generally has a negative connotation, we argue that it is crucial to distinguish its potential manifestations. The present study analyzes the impact of perceived elite polarization on political support by disentangling the effects of elite incivility from those of ideological polarization, and, additionally, by analyzing different dimensions of ideological polarization (i.e., along a general left–right, economic, and cultural dimension). Using survey data from the Dutch Parliamentary Election Survey 2021, we find that perceived incivility has a negative impact on political support. In contrast, perceived left–right polarization and economic issue polarization have a positive effect on political support, while cultural polarization has no effect. These findings show that elite polarization can convey both perceptions of conflict and choice to citizens, and that its impact on political support crucially depends on the dimension of polarization under study. Our study thereby refines our knowledge of the attitudinal consequences of elite polarization.