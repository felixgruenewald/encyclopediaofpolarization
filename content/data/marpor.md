---
name: MARPOR
title: Manifesto
link: 
- https://manifestoproject.wzb.eu
---
## Description

The Manifesto Research on Political Representation (MARPOR), formerly known and still often referred to as the "Manifesto Project", provides a comprehensive collection and content analysis of party election manifestos, including over 1000 parties from 50 countries worldwide. Because the collected manifestos are content analyzed, MARPOR offers a wide range of topics related to party preferences, party program offerings, the relationship between parties and voters, or the translation of party programs into policies.

With regard to polarization, MARPOR includes items and information that can be used to measure ideological polarization at the elite level. 

## Measures
Measures that use this dataset:

| Measure                                             | Polarization | Level |
| --------------------------------------------------- | ------------ | ----- |
| [Dispersion]({{< ref "measures/dispersion.md" >}}) | ideological  | elite |
| [Range]({{< ref "measures/range.md" >}})                        | ideological  | elite |
| [Variance]({{< ref "variance-weighted" >}})                     | ideological  | elite |

## Visualization
{{< shinyapp "marpor" "sd_expert_parties" >}}

## Use cases
Publications that use this dataset:

| Title                                                                                                   | Authors                      |
| ------------------------------------------------------------------------------------------------------- | ---------------------------- |
| [The Spatial Structure of Party Competition]({{< ref "usecases/andrews_spatial_2009.md" >}})                        | Andrews and Money (2009)     |
| [Political polarisation and environmental attitudes]({{< ref "usecases/birch_political_2020.md" >}})                | Birch (2020)                 |
| [Does voter polarisation induce party extremism?]({{< ref "usecases/dreyer_does_2019.md" >}})                       | Dreyer and Bauer (2019)      |
| [The Variance Matters]({{< ref "usecases/ezrow_variance_2007.md" >}})                                               | Ezrow (2007)                 |
| [Does Austerity Cause Polarization?]({{< ref "usecases/hübscher_does_2023.md" >}})                                  | Hübscher et al. (2023)       |
| [Political Polarization and Cabinet Stability in Multiparty Systems]({{< ref "usecases/maoz_political_2010.md" >}}) | Maoz and Sommer-Topcu (2010) |
| [Electoral Rule Disproportionality and Platform Polarisation]({{< ref "usecases/matakos_electoral_2016.md" >}})     | Matakos et al. (2016)        |
| [Economic Integration, Party Polarisation and Electoral Turnout]({{< ref "usecases/steiner_economic_2012.md" >}})   | Steiner and Martin (2012)    |
| [Polarization, Number of Parties, and Voter Turnout]({{< ref "usecases/wilford_polarization_2017.md" >}})           | Wilford (2017)               |

