---
title: Definitions of Polarization
---

As summarized in the figure below, the most important conceptual distinctions when discussing polarization are whether one is concerned with [ideological]({{< ref "ideological" >}}) or [affective]({{< ref "affective" >}}) polarization, and whether parties or politicians ([elite]({{< ref "elite" >}}) level) or individual citizens ([mass]({{< ref "mass" >}}) public) are the unit of analysis. Another subordinate classification distinguishes between [horizontal]({{< ref "horizontal" >}}), partisans disliking partisans of other parties, and [vertical]({{< ref "vertical" >}}) affective polarization, partisan disliking other parties or their elites ([Röllicke 2023]({{< ref "rollicke_polarisation_2023" >}})) . This latter distinction does have important implications for measurement.

![](/images/f_polarization_diagram.png)

Finally, it is worth noting that polarization can denote both a state (of divisions in society) or a process (moving towards greater divisions in society) (see also [Bramson et al. 2017]({{< ref "bramson_understanding_2017" >}})). Nevertheless, existing empirical measures of polarization are mostly implicitly, sometimes explicitly based on an understanding of polarization as a state. However, these measures, if measured over multiple time points, lend themselves to capturing a process of increasing, stable, or decreasing polarization.

{{< citation >}}