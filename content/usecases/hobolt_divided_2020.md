---
authors: Sara B. Hobolt, Thomas J. Leeper, James Tilley
date: 2020-01-01
title: Divided by the Vote
subtitle: Affective Polarization in the Wake of the Brexit Referendum
publication: British Journal of Political Science
doi: https://doi.org/10.1017/S0007123420000125
polarisation:
  - affective
  - mass
  - horizontal
measures: 
data:
  - other
aliases:
  - Hobolt et al. (2020)
---

## Abstract
A well-functioning democracy requires a degree of mutual respect and a willingness to talk across political divides. Yet numerous studies have shown that many electorates are polarized along partisan lines, with animosity towards the partisan out-group. This article further develops the idea of affective polarization, not by partisanship, but instead by identification with opinion-based groups. Examining social identities formed during Britain's 2016 referendum on European Union membership, the study uses surveys and experiments to measure the intensity of partisan and Brexit-related affective polarization. The results show that Brexit identities are prevalent, felt to be personally important and cut across traditional party lines. These identities generate affective polarization as intense as that of partisanship in terms of stereotyping, prejudice and various evaluative biases, convincingly demonstrating that affective polarization can emerge from identities beyond partisanship.