---
authors: Enrique Hernández, Eva Anduiza, Guillem Rico
date: 2021-01-01
title: Affective polarization and the salience of elections
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2020.102203
polarisation:
  - affective
  - mass
  - vertical
measures:
  - distance
data:
  - cses
aliases:
  - Hernández et al. (2021)
---

## Abstract
In this article we analyze the effects of election salience on affective polarization. Campaigns and elections epitomize the moment of maximum political conflict, information spread, mobilization, and activation of political identities and predispositions. We therefore expect that affective polarization will be higher just after an election has taken place. By the same token, as elections lose salience, affective polarization will diminish. We analyze this question using CSES data from 99 post-electoral surveys conducted in 42 countries between 1996 and 2016. Our identification strategy exploits variation in the timing of survey interviews with respect to the election day as an exogenous measure of election salience. The empirical findings indicate that as elections lose salience affective polarization declines. The article further contributes to the debate on the origins of affective polarization by exploring two mechanisms that may account for this relationship: changes in ideological polarization and in the intensity of party identification. Both are relevant mediators, with ideological polarization seemingly playing a more important role.