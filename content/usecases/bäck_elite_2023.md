---
authors: Hanna Bäck, Royce Carroll, Emma Renström, Alexander Ryan
date: 2023-01-01
title: Elite communication and affective polarization among voters
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102639
polarisation:
  - affective
  - mass
  - horizontal
measures:
  - distance
data:
  - other
aliases:
  - Bäck et al. (2023)
---

## Abstract

How does elite communication influence affective polarization between partisan groups? Drawing on the literature on partisan source cues, we expect that communication from in- or outgroup party representatives will increase affective polarization. We argue that polarized social identities are reinforced by partisan source cues, which bias perceptions of elite communication and result in increased intergroup differentiation. Further, we expect that the effect of such source cues is greater for voters with stronger partisan affinities. To evaluate our hypotheses, we performed a survey experiment among about 1300 voters in Sweden. Our analyses show that individuals who received a factual political message with a source cue from an in- or outgroup representative exhibited higher affective polarization, especially when they already held strong partisan affinities. This suggests that political elites can increase affective polarization by reinforcing existing group identities, and that this occurs in conjunction with biased interpretation of elite communication. The results improve our understanding of how political elites can influence affective polarization and add to previous research on party cues and attitude formation by demonstrating that such source cues can also increase intergroup differentiation.