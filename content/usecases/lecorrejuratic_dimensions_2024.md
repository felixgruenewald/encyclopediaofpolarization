---
authors: Morgan Le Corre Juratic
date: 2024-01-01
title: Dimensions of polarization, realignment and electoral participation in Europe. The mobilizing power of the cultural dimension
subtitle:
publication: European Journal of Political Research
doi: https://doi.org/10.1111/1475-6765.12718
polarisation:
  - elite
  - issue
  - vertical
measures:
  - polarization-index
data:
  - ches
aliases:
  - Le Corre Jurratic (2024)
tags:
---

## Abstract
Over the past two decades, extreme parties have gained increasing electoral success in European party systems. While this party polarization is often associated with its negative consequences, recent studies have suggested its potential benefit for remobilizing the electorate by offering clear political alternatives. However, it remains unclear which groups of citizens may be mobilized by broader supply and whether this positive effect is generalizable to multiparty systems. This article contributes to this debate arguing that the system multidimensionality matters when assessing the relationship between polarization and voter turnout. Through a multilevel analysis and two studies at the aggregate and individual levels, this article provides evidence that party polarization is associated with increased turnout only when parties polarize on the cultural dimension of party competition. This effect is moderated by the party system unidimensionality and mobilizes voters at large, regardless of their level of extremism, political awareness or partisanship. These findings support previous research suggesting a ‘realignment’ of party systems, meaning that the main line of political conflict for parties and voters is shifting towards the cultural dimension of party competition across Europe.