---
authors: Eelco Harteveld, Markus Wagner
date: 2023-01-01
title: Does affective polarisation increase turnout?
subtitle: Evidence from Germany, The Netherlands and Spain
publication: West European Politics
doi: https://doi.org/10.1080/01402382.2022.2087395
polarisation:
  - affective
  - mass
  - vertical
  - horizontal
measures:
  - spread
data:
  - other
aliases:
  - Harteveld and Wagner (2023)
---

## Abstract
Polarisation is often seen as mainly negative for the functioning of democracies, but one of its saving graces could be that it raises the stakes of politics and encourages participation. This study explores the relationship between affective polarisation and turnout using three longitudinal designs. It makes use of three decades of repeated cross-sectional surveys in Germany, a two-wave panel study in Spain, and an eleven-wave panel study in the Netherlands. It tests whether affective polarisation increases turnout using varying operationalizations and specifications, and studies whether any boost in participation extends beyond the most politically sophisticated citizens. The findings suggest a sizeable independent effect of affective polarisation on turnout even when accounting for reverse causality and for the confounding impact of positive partisanship and ideological polarisation. Importantly, this effect might even be somewhat more pronounced among those who are least sophisticated. The concluding section discusses the normative and theoretical implications of these findings.