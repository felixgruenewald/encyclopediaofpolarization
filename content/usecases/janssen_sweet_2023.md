---
authors: Lisa Janssen
date: 2023-01-01
title: Sweet victory, bitter defeat
subtitle: The amplifying effects of affective and perceived ideological polarization on the Winner–Loser gap in political support
publication: European Journal of Political Research
doi: https://doi.org/10.1111/1475-6765.12625
polarisation:
  - ideological
  - affective
  - mass
  - vertical
measures:
  - spread
data:
  - other
aliases:
  - Janssen (2023)
---

## Abstract
Accepting defeat in the aftermath of elections is crucial for the stability of democracies. But in times of intense polarization, the voluntary consent of electoral losers seems less obvious. In this paper, I study whether affective and perceived ideological polarization amplify the winner–loser gap in political support. Using multilevel growth curve modelling on pre and post-election panel data from the British Election Study Internet Panel collected during the 2015 and 2019 UK general elections, I show that the winner–loser gap is indeed more pronounced amongst voters with higher levels of affective and perceived ideological polarization. Moreover, the results illustrate that polarized voters experience a stark decrease in their support for the political system following their electoral loss. Given the high and, in some Western democracies, rising polarization levels, these findings have important implications for losers’ consent and the stability of democracies in election times.