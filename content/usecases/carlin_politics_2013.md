---
authors: Ryan E. Carlin, Gregory J. Love
date: 2013-01-01
title: The Politics of Interpersonal Trust and Reciprocity
subtitle: An Experimental Approach
publication: Political Behavior
doi: https://doi.org/10.1007/s11109-011-9181-x
polarisation:
  - affective
  - mass
  - horizontal
measures: 
data:
  - other
aliases:
  - Carlin and Love (2013)
---

## Abstract
Trust and reciprocity are theoretically essential to strong democracies and efficient markets. Working from the theoretical frameworks of social identity and cognitive heuristics, this study draws on dual-process models of decision making to expect (1) the trustor to infer trustworthiness from partisan stereotypes and thus to discriminate trust in favor of co-partisans and against rival partisans, but (2) the trustee to base reciprocity decisions on real information about the trustor’s deservingness rather than a partisan stereotype. So whereas partisanship is likely to trigger trust biases, the trust decision itself provides enough information to override partisan biases in reciprocity. The analysis derives from a modified trust game experiment. Overall, the results suggest partisanship biases trust decisions among partisans, and the degree of partisan trust bias is consistent with expectations from both social identity theory and cognitive heuristics. When it comes to reciprocity, however, information about the other subject’s level of trust nullifies partisan bias.