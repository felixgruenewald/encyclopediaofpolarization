---
authors: F. Ellger
date: 2024-01-01
title: The Mobilizing Effect of Party System Polarization. Evidence From Europe
subtitle:
publication: Comparative Political Studies
doi: https://doi.org/10.1177/00104140231194059
polarisation:
  - affective
  - elite
  - ideological
measures:
  - polarization-index
data:
  - other
  - marpor
aliases:
  - Ellger (2024)
tags:
---

## Abstract
Does party system polarization mobilize voters? Polarization is increasingly shaping democratic competition across Europe. While often perceived to be negative, polarization can be an effective remedy against voter disengagement. This paper investigates two distinct, but often conflated mechanisms, which could explain why polarization leads to mobilization. Spatial polarization of parties diversifies electoral options at the ballot, while affective polarization mobilizes based on emotional considerations. This article then shows the link between polarization and turnout across 22 European countries. The results are complemented by a difference-in-differences analysis of German local elections. However, voting results alone do not inform about the mechanism at play. Survey data is used to show that negative affect appears to be the main driver of voter participation. Party polarization thus has ambivalent consequences for democracies: It mobilizes the electorate, but its effect is driven by negative emotions.