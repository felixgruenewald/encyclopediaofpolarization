---
authors: Noam Gidron, Lior Sheffer, Guy Mor
date: 2022-01-01
title: Validating the feeling thermometer as a measure of partisan affect in multi-party systems
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2022.102542
polarisation:
  - affective
  - mass
  - vertical
measures:
  - socialdistance
data:
  - other
aliases:
  - Gidron et al. (2022)
---

## Abstract
Affective polarization is increasingly studied comparatively, and virtually all studies that do so operationalize it using the feeling thermometer. Yet this survey instrument has not yet been validated in a multi-party context. We argue that for the thermometer to be a valid measure of partisan affect also in multi-party systems, it needs to capture sentiment towards partisans and to correlate with other measures of affective polarization. Using panel study fielded throughout Israel’s elections in 2019–2020, we show that both requirements hold. We use text analysis to substantiate that thermometer scores reflect sentiment towards party supporters, and demonstrate that they go hand-in-hand with preferences for social distance and discrimination in economic games. We discuss implications for the study of affective polarization.