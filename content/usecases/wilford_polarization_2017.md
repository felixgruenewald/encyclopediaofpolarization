---
authors: Allan M. Wilford
date: 2017-01-01
title: Polarization, Number of Parties, and Voter Turnout
subtitle: Explaining Turnout in 26 OECD Countries
publication: Social Science Quarterly
doi: https://doi.org/10.1111/ssqu.12366
polarisation:
  - ideological
  - elite
measures:
  - sd
data:
  - marpor
aliases:
  - Wilford (2017)
---

## Abstract
Objectives The objective of this study is to explore how party systems can affect turnout by exploring the conditional effect of number of parties and party polarization on democracies. Methods Using Comparative Manifesto Project data from 26 democracies, this study develops a measure of party systems that interacts party polarization and number of parties to explain turnout. Results Findings show that the composition of the party system as a whole is a key determinate of a voter's propensity to vote. Highly polarized systems with few parties spur individuals to vote, while low levels of polarization and many parties reduce incentives to vote. Conclusions Results have important implications for theories of turnout, resolving the confusion surrounding how party systems affect political participation.
