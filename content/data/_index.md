---
title: Datasets to Measure Polarization
---

To obtain party positions for comparative research, researchers can rely on the Comparative Study of Electoral Systems ([CSES]({{< ref "cses" >}})), for national elections, and the European Election Study ([EES]({{< ref "ees" >}})), for elections to the Euro- pean parliament, which provides respondents’ ratings of party positions, the Manifesto Project Dataset ([MARPOR]({{< ref "marpor" >}})), which provides party positions based on content analyses of the parties’ election manifestos, or the Chapel Hill Expert Survey ([CHES]({{< ref "ches" >}})), which provides expert-coded party positions (for a summary of sources, see Table below). For measuring ideological polarization at the mass level, the CSES, the Eurobarometer ([EB]({{< ref "eb" >}})) surveys, the European Social Survey ([ESS]({{< ref "ess" >}})) and the EES provide data on the ideological self-positioning of citizens for many countries and at several points in time. However, the CSES is the only large-scale, cross-national, and publicly available survey that includes the party thermometer questions needed to calculate the most common measures of affective polarization.

_Table: Available data sources for measuring different types of polarization at different levels of measurement for cross-national comparative research._

|       | Ideological             | Affective |
| ----- | ----------------------- | --------- |
| Elite | CHES, MARPOR, CSES, EES |           |
| Mass  | CSES, EB, EES, ESS      | CSES      |


Given the intention to measure a particular type of polarization – either ideological or affective – at a particular level – elite or mass – researchers still face several measurement choices. First, whether to consider polarization between all parties simultaneously, only between one party and all other parties, or, more recently, between party dyads (for the latter see [Gidron, Sheffer, and Mor (2022)]({{< ref "gidron_validating_2022" >}})). Second, whether to weight by party size, and third, of course, which input data to use. At the time of writing, the dominant approach in the literature seems to be not to rely on identifying a single outparty and to account for the importance of parties by calculating weighted measures. Among the data sources, the CSES is the most popular. Its appeal lies in the ease with which elite and mass polarization measures can be computed from the same dataset and its relatively broad geographic and temporal coverage.

{{< citation >}}