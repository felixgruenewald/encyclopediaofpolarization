---
authors: J.B. Phillips
date: 2024-01-01
title: Affective polarization and habits of political participation
subtitle: 
publication: Electoral Studies
doi: https://doi.org/10.1016/j.electstud.2023.102733
polarisation:
  - affective
  - horizontal
measures:
  - distance
data:
  - other
aliases:
  - Phillips (2024)
tags:
---

## Abstract
Affective polarization, or relative dislike of opposing partisans, is associated with several negative outcomes for democracy. However, a number of studies argue that affective polarization has one positive democratic consequence: it spurs political participation. However, political participation, especially voting, is habitual, and the factors that spur people to start participating are not the same as those that sustain participation once it is initiated. Existing work does not address this distinction. Leveraging large-scale survey data linked to validated measures of turnout as well as panel data, this paper shows that affective polarization mainly serves to sustain existing habits of turnout. In contrast, there is little evidencethat affective polarization motivates people who did not previously participate to begin doing so. These results indicate that instead of improving democratic outcomes, affective polarization exacerbates existing inequities in political participation