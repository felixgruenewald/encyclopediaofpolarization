---
authors: Costin Ciobanu, Dani Sandu
date: 2022-01-01
title: What can De-Polarize the Polarizers?
subtitle: Affective Polarization for Party Activists Before and After Elections
publication: Working Paper
doi: 
polarisation:
  - affective
  - mass
  - horizontal
  - vertical
measures:
  - distance
data:
  - other
aliases:
  - Ciobanu and Sandu (2022)
---

## Abstract
Aﬀective polarization has become an extremely important topic of research in the social sciences. The larger reason for this growing importance is the fact that many researchers have identiﬁed polarization as problematic for democracy and disruptive for society in general. Because the increase of polarization is so dangerous, its mitigation becomes more urgent. Further, recent research suggests that the main agents driving polarization are party activists. Still, little is known about aﬀective polarization in activists outside the US, especially because they are diﬃcult to survey in a longitudinal structure. We propose to address this gap. We claim that cross-party contact might reduce aﬀective polarization among party activists even in less established democracies, following up on recent work concerning US political parties. We leverage a pre-registered unique longitudinal dataset of party activists in a multi-party European post-communist democracy using a diﬀerence-in-diﬀerence framework. Speciﬁcally, we employ a threewave panel with approximately 8,300 responses from members of a new political party, collected immediately before and after the December 2020 Romanian legislative elections. We study whether contact between strong partisans who act as party delegates in the precincts on election day reduces aﬀective polarization, while comparing with strong partisans without delegate responsibilities. We demonstrate that party activists are aﬀectively polarized and that they are mostly polarized against the out-party elites and the party itself. Election-day contact with out-party peers has a limited eﬀect on reducing animus towards the members and voters of the party the partisans are most polarized against, but it can decrease polarization towards the party itself. Similar to the mass public, party activists de-polarize immediately after elections and the eﬀect persists for at least two months.