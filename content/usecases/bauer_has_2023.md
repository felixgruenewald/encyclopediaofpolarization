---
authors: Paul C. Bauer, Davide Morisi
date: 2023-01-01
title: Has Trust in the European Parliament Polarized?
subtitle: 
publication: Socius
doi: https://doi.org/10.1177/23780231231175430
polarisation:
  - ideological
  - mass
measures: 
data:
  - ess
aliases:
  - Bauer and Morisi (2023)
---

## Abstract
Scholars usually investigate how average levels of trust in institutions vary across countries and over time. Focusing on average levels, however, ignores distributional properties that might be equally relevant for institutional legitimacy and, more broadly, democratic stability. In this study, the authors investigate how the distribution of trust in the European Parliament has changed over time and across European Union member states. Drawing on pooled cross-sectional data from the European Social Survey for the period from 2002 to 2020, the authors find that confidence in the European Parliament has not only declined over time but also polarized as citizens have increasingly moved away from the “average citizen.” Furthermore, the authors find that trust has polarized, especially among the young versus the elderly and the employed versus the unemployed. These findings have implications for the legitimacy of European Union institutions.